package docker

import (
	"errors"
)

// 检查连接
func (c *Client) CheckCli() error {
	if c.Cli == nil {
		return errors.New("Docker连接尚未就绪")
	} else {
		return nil
	}
}
