package gomicro

// [必须覆盖] 全局日志
var DefaultLogger Logger = nil

type Logger interface {
	Info(info string, fields ...interface{})
	Error(err error, info string, fields ...interface{})
	Fatal(err error, info string, fields ...interface{})
}
