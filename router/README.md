# network

路由相关函数。

* `Router`：标准路由，有参数和返回值。
* `RouterNoResponse`：无返回值的路由。
* `RouterFile`：文件路由，参数放在`RouterFileParamKey`键中。
* `RouterFileNoResponse`：无返回值的文件路由，参数放在`RouterFileParamKey`键中。
* `Recovery`：`gin`路由的全局崩溃恢复中间件。
