/*
 * 访问frps的stcp服务时，需要一个visitor连接
 */

package frp

import (
	"math/rand"
	"sync"
	"time"

	"github.com/fatedier/frp/client"
	"github.com/fatedier/frp/pkg/config"
	"github.com/fatedier/frp/pkg/consts"
	"github.com/fatedier/frp/pkg/util/log"
	"github.com/fatedier/golib/crypto"
)

// frps的visitor连接使用的端口池单例
var VisitorPortManager = PortManager{
	MinPort: 0, // 必须外部更改!!!
	MaxPort: 0, // 必须外部更改!!!
	lock:    sync.RWMutex{},
	status:  map[uint16]bool{},
	pointer: 0,
}

// frps的visitor连接配置信息
type VisitorClientConfig struct {
	ServerName      string // 服务器名称
	ServerPort      uint16 // 服务器端口号
	ServerToken     string // 服务器Token
	ClientName      string // 客户端名称(server_name)
	ClientSecretKey string // 客户端密钥(secret_key)
}

// frps的visitor连接
type VisitorClient struct {
	Config  VisitorClientConfig // 连接配置信息
	Visitor *client.Service     // visitor服务
	Port    uint16              // visitor服务端口号
}

// 新建函数
func NewVisitorClient(config VisitorClientConfig) (c *VisitorClient) {
	c = new(VisitorClient)
	c.Config = config
	c.Visitor = nil
	c.Port = 0
	return
}

// visitor连接到frps
func (c *VisitorClient) Connect() (err error) {
	if c.Visitor != nil || c.Port > 0 {
		c.Close()
	}
	// 获取一个端口
	port, err := VisitorPortManager.Get()
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			VisitorPortManager.Put(port)
		}
	}()
	// 配置信息
	clientCfg := config.GetDefaultClientConf()
	clientCfg.ServerAddr = c.Config.ServerName
	clientCfg.ServerPort = int(c.Config.ServerPort)
	clientCfg.Token = c.Config.ServerToken
	clientCfg.LoginFailExit = true
	cfg := &config.STCPVisitorConf{}
	cfg.ProxyType = consts.STCPProxy
	cfg.Role = "visitor"
	cfg.ServerName = c.Config.ClientName
	cfg.Sk = c.Config.ClientSecretKey
	cfg.BindAddr = "127.0.0.1"
	cfg.BindPort = int(port)
	cfg.UseCompression = true
	visitorCfgs := map[string]config.VisitorConf{
		c.Config.ServerName: cfg,
	}
	// visitor连接
	svr, err := client.NewService(clientCfg, nil, visitorCfgs, "")
	if err != nil {
		return
	}
	// 记录信息
	c.Visitor = svr
	c.Port = port
	// 启动连接
	go func() {
		if e := svr.Run(); e != nil {
			c.Close()
		}
	}()
	time.Sleep(100 * time.Millisecond)
	return
}

// 关闭visitor连接
func (c *VisitorClient) Close() {
	if c.Visitor != nil {
		c.Visitor.Close()
		c.Visitor = nil
	}
	if c.Port > 0 {
		VisitorPortManager.Put(c.Port)
		c.Port = 0
	}
}

func init() {
	crypto.DefaultSalt = "frp"
	rand.Seed(time.Now().UnixNano())
	log.SetLogLevel("error")
}
