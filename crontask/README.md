# crontask

定时任务库，支持到秒。

使用方法：

```go
crontask.Register(crontask.Task{
    Name: "任务1",
    Time: "0 */10 * * * *",
    Handler: handler,
})

crontask.Init()
```
