package docker

import (
	"context"
	"encoding/base64"
	"encoding/json"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	dockerCli "github.com/docker/docker/client"
)

// Docker连接
type Client struct {
	Ctx context.Context   // 网络请求上下文
	Cli *dockerCli.Client // docker库的连接
}

// 新建Docker连接
func NewClient(ctx context.Context) *Client {
	c := new(Client)
	c.Ctx = ctx
	c.Cli = nil
	return c
}

// 连接操作
// host有值(例如"http://127.0.0.1:1234")，则连接到远程docker上
// host为空，则连接到/var/run/docker.sock上
func (c *Client) Connect(host string) (err error) {
	if c.Cli != nil {
		c.Close()
	}
	opts := []dockerCli.Opt{}
	if len(host) > 0 {
		opts = append(opts, client.WithHost(host))
	}
	opts = append(opts, client.WithAPIVersionNegotiation())
	cli, err := dockerCli.NewClientWithOpts(opts...)
	if err != nil {
		return
	}
	c.Cli = cli
	return
}

// 断开操作
func (c *Client) Close() {
	if c.Cli != nil {
		_ = c.Cli.Close()
		c.Cli = nil
	}
}

// 简易的用户名+密码鉴权信息
func (c *Client) GenerateUserPwdAuth(
	username string,
	password string,
) string {
	if len(username) == 0 || len(password) == 0 {
		return ""
	}
	authConfig := types.AuthConfig{
		Username: username,
		Password: password,
	}
	encodedJSON, _ := json.Marshal(authConfig)
	return base64.URLEncoding.EncodeToString(encodedJSON)
}
