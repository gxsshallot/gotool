package autohttp

import "context"

// 状态类的回调，需要用户自己实现
type StatusCallbackInterface interface {
	// 如果要实现发送前修改Item，则复写此方法
	OnTaskBuilding(c context.Context, item *Item)
	// 结果判断
	OnTaskResultValid(c context.Context, item Item, statusCode int, resultBytes []byte) (err error)
	// 更新任务状态
	OnTaskStatusUpdate(c context.Context, item Item, errMessage string)
}

// 全局状态类回调方法
var StatusCallback StatusCallbackInterface = nil
