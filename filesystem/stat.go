package filesystem

import (
	"os"
)

// 判断是否是文件
func IsFile(path string) (isFile bool, err error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return
	}
	isFile = !fileInfo.IsDir()
	return
}

// 判断是否是目录
func IsDir(path string) (isDir bool, err error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return
	}
	isDir = fileInfo.IsDir()
	return
}
