# frp

使用frp管理远程端口。

* 服务端下发指令，每个指令包括全部的反向代理信息。
* 客户端监听指令，根据下发的反向代理信息启动frpc连接。

目前支持如下的反向代理模式：

* `http`：将`IP:PORT`映射为云端的HTTP服务，需要一个自定义域名，访问时可以直接域名访问。
* `tcp`/`udp`：基本连接，将`IP:PORT`映射到云端的指定端口，这个端口是由云端的`ExternalPortManager`统一分配的，访问时，使用云端服务器IP/域名+分配的端口号，进行访问。
* `stcp`：特指将本地的`/var/run/docker.sock`，用`stcp`的方式映射到云端，本地的服务有密钥，使用`visitor`连接时，必须密钥一致。`visitor`会将`IP:PORT`映射到云端服务容器内部的`127.0.0.1`的一个端口上，由`VisitorPortManager`分配端口号。然后可以使用`docker`部分的服务连接这个端口号，就可以对本地进行Docker操作了。

需要设置端口池范围：

```go
// 设置TCP/UDP外部端口范围，二者共用这个区间
frp.ExternalPortManager.MinPort = xxx
frp.ExternalPortManager.MaxPort = xxx

// (可选) 设置visitor端口范围，仅在有stcp的docker.sock映射时使用
frp.VisitorPortManager.MinPort = 10000
frp.VisitorPortManager.MaxPort = 60000
```

需要初始化TCP/UDP端口池的使用情况(与数据库中同步)：

```go
frp.ExternalPortManager.Init(xxx)
```

注意事项：

* `frps`和`frpc`版本为`0.33.0`。
* `github.com/fatedier/frp`：版本`v0.37.1`，因为最新的`v0.58.1`需要使用Go的`1.22`特性，包括但不限于`cmp`、`slices`、`math/rand/v2`等。
