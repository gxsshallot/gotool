package docker

import (
	"io"

	"github.com/ahmetalpbalkan/dlog"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	specs "github.com/opencontainers/image-spec/specs-go/v1"
)

// 创建容器
func (c *Client) ContainerCreate(
	containerName string,
	config *container.Config,
	hostConfig *container.HostConfig,
	networkingConfig *network.NetworkingConfig,
	platformConfig *specs.Platform,
) (rsp container.CreateResponse, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if rsp, err = c.Cli.ContainerCreate(c.Ctx, config, hostConfig, networkingConfig, platformConfig, containerName); err != nil {
		return
	}
	return
}

// 启动容器
func (c *Client) ContainerStart(
	containerId string,
	options types.ContainerStartOptions,
) (err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if err = c.Cli.ContainerStart(c.Ctx, containerId, options); err != nil {
		return
	}
	return
}

// 停止容器
func (c *Client) ContainerStop(
	containerId string,
	options container.StopOptions,
) (err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if err = c.Cli.ContainerStop(c.Ctx, containerId, options); err != nil {
		return
	}
	return
}

// 移除容器
func (c *Client) ContainerRemove(
	containerId string,
	options types.ContainerRemoveOptions,
) (err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if err = c.Cli.ContainerRemove(c.Ctx, containerId, options); err != nil {
		return
	}
	return
}

// 获取所有容器信息
func (c *Client) ContainerList(
	options types.ContainerListOptions,
) (list []types.Container, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if list, err = c.Cli.ContainerList(c.Ctx, options); err != nil {
		return
	}
	return
}

// 获取容器信息
func (c *Client) ContainerInspect(
	containerId string,
) (rsp types.ContainerJSON, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	if rsp, err = c.Cli.ContainerInspect(c.Ctx, containerId); err != nil {
		return
	}
	return
}

// 查看容器日志
func (c *Client) ContainerLogs(
	containerId string,
	options types.ContainerLogsOptions,
) (out string, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	outInfo, err := c.Cli.ContainerLogs(c.Ctx, containerId, options)
	if err != nil {
		return
	}
	defer outInfo.Close()
	outBytes, _ := io.ReadAll(dlog.NewReader(outInfo))
	out = string(outBytes)
	return
}
