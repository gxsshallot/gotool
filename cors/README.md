# cors

gin的中间件，启用CORS支持，基于[gin-contrib/cors](https://github.com/gin-contrib/cors)的`1.7.2`版本。

将其中的`gin`，替换为`gotool`的精简版`gin`，其他无修改。
