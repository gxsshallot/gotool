# sse

基于`github.com/r3labs/sse`的`v2`版本，封装的`sse`服务端和客户端库。

使用方法(服务端)：

```go
SSE.Instance.AutoStream = true
SSE.Instance.AutoReplay = false

// gin使用
g.GET("sse", gin.WrapF(SSE.Instance.ServeHTTP))
```

目前客户端连接服务端尚未测通。
