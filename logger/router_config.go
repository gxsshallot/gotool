package logger

import (
	"path"
	"strings"

	"gitee.com/gxsshallot/gotool/gin"
)

// 忽略时替换为的字符串
const IgnoreReplaceStr = "-"

// 路由配置信息项
type RouterConfig struct {
	DisableReq          bool                              // 禁用请求
	DisableRsp          bool                              // 禁用应答
	HeaderFields        []string                          // (通用) 输出的header字段
	IgnoreReqBodyFields []string                          // 忽略的body字段，不同的Content-Type对应不同的处理
	IgnoreRspFields     []string                          // 忽略的rsp字段，不同的Content-Type对应不同的处理
	OutputReqBody       func(*gin.Context, string) string // 自定义输出请求的body函数
	OutputRspBody       func(*gin.Context, string) string // 自定义输出应答的body函数
}

// 全局配置项
var GlobalRouterConfig = RouterConfig{
	DisableReq:          false,
	DisableRsp:          false,
	HeaderFields:        nil,
	IgnoreReqBodyFields: nil,
	IgnoreRspFields:     nil,
	OutputReqBody:       nil,
	OutputRspBody:       nil,
}

// 路由自定义配置映射表
var routerConfigMap = map[string]map[string]RouterConfig{}

// 整个方法的禁用信息映射
var routerMethodDisabledMap = map[string]bool{}

// 设置整个方法的禁用信息
func SetRouterMethodDisabled(method string, status bool) {
	method = strings.ToUpper(method)
	routerMethodDisabledMap[method] = status
}

// 设置自定义映射信息
func SetRouterConfigDirectly(
	method string,
	urlPath string,
	config RouterConfig,
) {
	method = strings.ToUpper(method)
	if _, ok := routerConfigMap[method]; ok {
		routerConfigMap[method][urlPath] = config
	} else {
		routerConfigMap[method] = map[string]RouterConfig{
			urlPath: config,
		}
	}
}

// 设置自定义映射信息
func SetRouterConfig(
	method string,
	baseUrl string,
	apiUrl string,
	config RouterConfig,
) {
	urlPath := baseUrl
	if apiUrl != "" {
		urlPath = path.Join(baseUrl, apiUrl)
		if apiUrl[len(apiUrl)-1] == '/' && urlPath[len(urlPath)-1] != '/' {
			urlPath = urlPath + "/"
		}
	}
	SetRouterConfigDirectly(method, urlPath, config)
}

// 获取路由配置信息
func GetRouterConfig(method string, urlPath string) RouterConfig {
	method = strings.ToUpper(method)
	if v, ok := routerMethodDisabledMap[method]; ok && v {
		return RouterConfig{
			DisableReq: true,
			DisableRsp: true,
		}
	}
	if v1, ok1 := routerConfigMap[method]; ok1 {
		if v2, ok2 := v1[urlPath]; ok2 {
			return mergeRouterConfig(v2)
		}
	}
	return GlobalRouterConfig
}

func mergeRouterConfig(origin RouterConfig) (item RouterConfig) {
	item = origin
	if len(item.HeaderFields) == 0 {
		item.HeaderFields = GlobalRouterConfig.HeaderFields
	}
	if len(item.IgnoreReqBodyFields) == 0 {
		item.IgnoreReqBodyFields = GlobalRouterConfig.IgnoreReqBodyFields
	}
	if len(item.IgnoreRspFields) == 0 {
		item.IgnoreRspFields = GlobalRouterConfig.IgnoreRspFields
	}
	return
}
