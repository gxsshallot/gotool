package logger

import (
	"encoding/json"
)

// 替换通用对象指定字段，必须是可以Json序列化的对象
func ReplaceObject(obj interface{}, replaceStr string, fields []string) string {
	str, _ := json.Marshal(obj)
	return string(processJsonString(str, false, replaceStr, fields))
}

// 替换Json字符串指定字段
func ReplaceString(jsonStr []byte, replaceStr string, fields []string) []byte {
	return processJsonString(jsonStr, false, replaceStr, fields)
}

// 删除通用对象指定字段，必须是可以Json序列化的对象
func DeleteObject(obj interface{}, fields []string) string {
	str, _ := json.Marshal(obj)
	return string(processJsonString(str, true, "", fields))
}

// 删除Json字符串指定字段
func DeleteString(jsonStr []byte, fields []string) []byte {
	return processJsonString(jsonStr, true, "", fields)
}
