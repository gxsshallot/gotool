package router

import (
	"encoding/json"
	"net/http"
	"time"

	"gitee.com/gxsshallot/gotool/gin"
)

// 返回结果
type Response[T any] struct {
	Status    bool   `json:"status"`    // 是否成功
	Message   string `json:"message"`   // 错误消息
	Data      T      `json:"data"`      // 数据区
	Timestamp int64  `json:"timestamp"` // 时间戳
}

// Json解码，为了兼容status=false时data为空字符串的情况
func (r *Response[T]) UnmarshalJSON(data []byte) error {
	var obj struct {
		Status    bool            `json:"status"`
		Message   string          `json:"message"`
		Data      json.RawMessage `json:"data"`
		Timestamp int64           `json:"timestamp"`
	}
	if err := json.Unmarshal(data, &obj); err != nil {
		return err
	}
	r.Status = obj.Status
	r.Message = obj.Message
	r.Timestamp = obj.Timestamp
	if !obj.Status {
		return nil
	}
	var objData T
	if err := json.Unmarshal(obj.Data, &objData); err != nil {
		return err
	}
	r.Data = objData
	return nil
}

// 返回成功消息
func Success(c *gin.Context, data interface{}) {
	if data == nil {
		data = gin.H{}
	}
	r := Response[any]{
		Status:    true,
		Message:   "",
		Data:      data,
		Timestamp: time.Now().Unix(),
	}
	c.JSON(http.StatusOK, r)
}

// 失败并终止中间件处理流程
func Failure(c *gin.Context, err error) {
	r := Response[any]{
		Status:    false,
		Message:   err.Error(),
		Data:      "",
		Timestamp: time.Now().Unix(),
	}
	c.AbortWithStatusJSON(http.StatusOK, r)
}
