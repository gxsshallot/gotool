package gomicro

import (
	"errors"
	"math/rand"
	"sync"
	"time"
)

// Next is a function that returns the next node
// based on the selector's strategy.
type Next func() (*RegistryNode, error)

var ErrNoneAvailable = errors.New("none available")

func init() {
	rand.Seed(time.Now().UnixNano())
}

// 随机算法
func Random(services []*RegistryService) Next {
	nodes := make([]*RegistryNode, 0, len(services))
	for _, service := range services {
		nodes = append(nodes, service.Nodes...)
	}
	return func() (*RegistryNode, error) {
		if len(nodes) == 0 {
			return nil, ErrNoneAvailable
		}
		i := rand.Int() % len(nodes)
		return nodes[i], nil
	}
}

// 轮询算法
func RoundRobin(services []*RegistryService) Next {
	nodes := make([]*RegistryNode, 0, len(services))
	for _, service := range services {
		nodes = append(nodes, service.Nodes...)
	}
	var i = rand.Int()
	var mtx sync.Mutex
	return func() (*RegistryNode, error) {
		if len(nodes) == 0 {
			return nil, ErrNoneAvailable
		}
		mtx.Lock()
		node := nodes[i%len(nodes)]
		i++
		mtx.Unlock()
		return node, nil
	}
}
