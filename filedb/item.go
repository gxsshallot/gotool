/*
 * 一张表对应一个文件，多用于记录大致固定数量的信息。
 * 例如某配置实体的列表等。
 */

package filedb

import (
	"encoding/json"
	"os"
	"path/filepath"
	"sync"
)

type Item[T any] struct {
	Filepath string       // 完整的文件路径
	Perm     os.FileMode  // 文件权限，默认0644
	lock     sync.RWMutex // 读写锁
}

// 新建一个文件存储
func NewItem[T any](dirpath string, filename string) *Item[T] {
	t := new(Item[T])
	t.Filepath = filepath.Join(dirpath, filename)
	t.Perm = 0644
	t.lock = sync.RWMutex{}
	return t
}

// 读取当前内容
func (t *Item[T]) Get() (data T, err error) {
	t.lock.RLock()
	defer t.lock.RUnlock()
	content, err := os.ReadFile(t.Filepath)
	if err != nil {
		return
	}
	if err = json.Unmarshal(content, &data); err != nil {
		return
	}
	return
}

// 写入新的内容
func (t *Item[T]) Set(data T) (err error) {
	t.lock.Lock()
	defer t.lock.Unlock()
	content, err := json.Marshal(data)
	if err != nil {
		return
	}
	if err = os.WriteFile(t.Filepath, content, t.Perm); err != nil {
		return
	}
	return
}
