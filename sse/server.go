package sse

import (
	"encoding/json"

	"github.com/r3labs/sse/v2"
)

// 服务端
type Server struct {
	Instance *sse.Server // 服务端实例
}

// 新建服务端
func NewServer() *Server {
	s := new(Server)
	s.Instance = sse.New()
	return s
}

// 注册一个信息流
func (s *Server) Register(stream string) *sse.Stream {
	return s.Instance.CreateStream(stream)
}

// 移除一个信息流
func (s *Server) Remove(stream string) {
	s.Instance.RemoveStream(stream)
}

// 往一个信息流里发消息
func (s *Server) Publish(stream string, data []byte) {
	s.Instance.Publish(stream, &sse.Event{
		Data: data,
	})
}

// 往一个信息流里发消息(JSON序列化)
func (s *Server) PublishJson(stream string, data interface{}) {
	bytes, _ := json.Marshal(data)
	s.Publish(stream, bytes)
}
