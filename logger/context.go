package logger

import (
	"context"
	"fmt"
	"time"
)

// 内部记录TrackId时使用的Key值
const KEY_ID = "TRACKIDENTIFIER"

// 生成新的Track对象
func NewContext() context.Context {
	return context.WithValue(context.Background(), KEY_ID, NewTrackId())
}

// 用已有ID生成新的Track对象
func NewContextWithId(trackId string) context.Context {
	return context.WithValue(context.Background(), KEY_ID, trackId)
}

// 获取日志ID
func GetTrackId(c context.Context) string {
	if c == nil {
		return ""
	}
	if trackId, ok := c.Value(KEY_ID).(string); ok {
		return trackId
	} else {
		return ""
	}
}

// 生成日志ID
func NewTrackId() string {
	now := time.Now()
	return fmt.Sprintf("%s%09d", now.Format("060102150405"), now.UnixNano()%1e9)
}
