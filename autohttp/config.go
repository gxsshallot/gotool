package autohttp

/*
主循环多长时间检查一次，单位秒，默认3秒。
每次检查需要轮询数据库，所以需要根据实际情况调整。
*/
var LoopInterval = int64(3)

/*
是否开启自动清理，默认不开启。
如果开启自动清理，则会根据选项不同，自动删除已完成记录或者未完成记录。
*/
var AutoCleanEnable = false

/*
自动清理循环的轮询时间间隔，单位秒，默认1小时。
*/
var AutoCleanLoopInterval = int64(3600)

/*
自动清理多久之前的成功记录，单位秒。
仅在AutoCleanEnable=true时有效。
大于0表示开启，等于0表示不开启。
*/
var AutoCleanSuccessTime = int64(0)

/*
自动清理多久之前的失败记录，单位秒。
仅在AutoCleanEnable=true时有效。
大于0表示开启，等于0表示不开启。
*/
var AutoCleanFailureTime = int64(0)

/*
自动清理多久之前的已停止记录，单位秒。
仅在AutoCleanEnable=true时有效。
大于0表示开启，等于0表示不开启。
*/
var AutoCleanStoppedTime = int64(0)

/*
默认重试类型。
true表示周期性重试，false表示阶梯性重试。
*/
var DefaultRetryType = true

/*
阶梯性重试默认间隔，单位秒，默认为空。
样例：[]int64{60, 5*60, 30*60, 1*3600}，表示在1分钟、5分钟、30分钟、1小时后重试。
*/
var RetryLadderDefaultInterval = []int64{}

/*
周期性重试默认间隔，单位秒，默认为60秒。
*/
var RetryStepDefaultInterval = int64(60)

/*
周期性重试次数，默认为0，0表示不限制次数。
*/
var RetryStepDefaultTime = int64(0)

/*
成功后是否写入任务，默认不开启。
*/
var TaskAddIfSuccess = false
