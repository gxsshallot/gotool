package autohttp

import (
	"context"
	"net/http"
)

// 发送GET请求
func Get(
	ctx context.Context,
	businessType string,
	businessId string,
	url string,
	header map[string]string,
) (statusCode int, rspBytes []byte, err error) {
	item := Item{
		BusinessType: businessType,
		BusinessId:   businessId,
		Url:          url,
		Method:       http.MethodGet,
		Header:       header,
		Body:         []byte{},
	}
	statusCode, rspBytes, err = apiBase(ctx, &item)
	return
}
