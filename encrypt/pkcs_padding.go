package encrypt

import (
	"bytes"
)

// PKCS填充（PKCS5和PKCS7的算法一样，只是填充字段多少的区别）
func PKCSPadding(plainText []byte, blockSize int) []byte {
	padding := blockSize - len(plainText)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(plainText, padText...)
}

// PKCS反填充（PKCS5和PKCS7的算法一样，只是填充字段多少的区别）
func PKCSUnPadding(cipherText []byte) []byte {
	length := len(cipherText)
	padding := int(cipherText[length-1])
	return cipherText[:(length - padding)]
}
