package imgresize

import (
	"bytes"
	"image/jpeg"
	"os"

	"github.com/nfnt/resize"
)

type ImageConverterJPG struct {
}

func (ImageConverterJPG) IsFormat(contentTypeName string) bool {
	return contentTypeName == "jpeg"
}

func (ImageConverterJPG) Convert(fileObj *os.File, params Params) (imageBuf bytes.Buffer, err error) {
	imgOrigin, err := jpeg.Decode(fileObj)
	if err != nil {
		return
	}
	canvas := resize.Thumbnail(params.Width, params.Height, imgOrigin, resize.Lanczos3)
	err = jpeg.Encode(&imageBuf, canvas, &jpeg.Options{
		Quality: params.Quality,
	})
	return
}
