package docker

import (
	"io"

	"github.com/docker/docker/api/types"
)

// 拉取镜像，可以使用GenerateUserPwdAuth生成用户名密码鉴权信息
func (c *Client) ImagePull(
	image string,
	options types.ImagePullOptions,
) (out string, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	outInfo, err := c.Cli.ImagePull(c.Ctx, image, options)
	if err != nil {
		return
	}
	defer outInfo.Close()
	outBytes, _ := io.ReadAll(outInfo)
	out = string(outBytes)
	return
}

// 获取镜像列表
func (c *Client) ImageList(
	options types.ImageListOptions,
) (list []types.ImageSummary, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	list, err = c.Cli.ImageList(c.Ctx, options)
	if err != nil {
		return
	}
	return
}

// 获取镜像信息
func (c *Client) ImageInspect(
	image string,
) (info types.ImageInspect, err error) {
	if err = c.CheckCli(); err != nil {
		return
	}
	info, _, err = c.Cli.ImageInspectWithRaw(c.Ctx, image)
	if err != nil {
		return
	}
	return
}
