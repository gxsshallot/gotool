package router

import (
	"bytes"
	"encoding/json"
)

// 调用官方Json库的默认反序列化函数时，会将长数字转为科学计数法的表示格式
func jsonUnmarshal(str []byte, target interface{}) (err error) {
	d := json.NewDecoder(bytes.NewReader(str))
	d.UseNumber()
	err = d.Decode(target)
	return
}
