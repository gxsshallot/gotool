package autohttp

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

// POST请求，application/x-www-form-urlencoded
func PostForm(
	ctx context.Context,
	businessType string,
	businessId string,
	apiUrl string,
	header map[string]string,
	body interface{},
) (statusCode int, rspBytes []byte, err error) {
	bodyData, err := json.Marshal(body)
	if err != nil {
		return
	}
	var bodyMap map[string]interface{}
	if err = json.Unmarshal(bodyData, &bodyMap); err != nil {
		return
	}
	bodyValues := url.Values{}
	for k, v := range bodyMap {
		bodyValues.Set(k, fmt.Sprintf("%v", v))
	}
	if header == nil {
		header = map[string]string{}
	}
	if _, ok1 := header["Content-Type"]; !ok1 {
		if _, ok2 := header["content-type"]; !ok2 {
			header["Content-Type"] = "application/x-www-form-urlencoded"
		}
	}
	item := Item{
		BusinessType: businessType,
		BusinessId:   businessId,
		Url:          apiUrl,
		Method:       http.MethodPost,
		Header:       header,
		Body:         []byte(bodyValues.Encode()),
	}
	statusCode, rspBytes, err = apiBase(ctx, &item)
	return
}
