package encoding

import (
	"bytes"
	"io"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

// UTF-8转换为GBK
func Utf8ToGbk(text []byte) (result []byte, err error) {
	var r bytes.Buffer
	writer := transform.NewWriter(&r, simplifiedchinese.GBK.NewEncoder())
	if _, err = writer.Write(text); err != nil {
		return
	}
	writer.Close()
	result = r.Bytes()
	return
}

// GBK转换为UTF-8
func GbkToUtf8(text []byte) (result []byte, err error) {
	reader := transform.NewReader(strings.NewReader(string(text)), simplifiedchinese.GBK.NewDecoder())
	result, err = io.ReadAll(reader)
	return
}
