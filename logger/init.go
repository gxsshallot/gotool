package logger

import (
	"path"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

var globalRotateLogs *rotatelogs.RotateLogs

// 初始化日志配置模块
func Init(logPath string) {
	logrus.ErrorKey = "e"
	stdLog := logrus.StandardLogger()
	stdLog.SetLevel(logrus.InfoLevel)
	stdLog.SetFormatter(&logrus.TextFormatter{
		DisableColors:   true,
		DisableQuote:    true,
		TimestampFormat: "20060102-150405",
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyTime:  "t",
			logrus.FieldKeyLevel: "l",
			logrus.FieldKeyMsg:   "m",
		},
	})
	// 日志组件Hook
	stdLog.AddHook(&ContextHook{})
	// Debug模式，直接输出到标准输出上
	if logPath == "" {
		return
	}
	// 按目录设置日志输出
	globalRotateLogs, _ = rotatelogs.New(
		path.Join(logPath, "%Y%m%d%H.log"),
		rotatelogs.WithRotationTime(time.Hour),
	)
	stdLog.AddHook(
		lfshook.NewHook(
			lfshook.WriterMap{
				logrus.InfoLevel:  globalRotateLogs,
				logrus.ErrorLevel: globalRotateLogs,
			},
			&logrus.TextFormatter{
				DisableColors:   true,
				DisableQuote:    true,
				TimestampFormat: "20060102-150405",
				FieldMap: logrus.FieldMap{
					logrus.FieldKeyTime:  "t",
					logrus.FieldKeyLevel: "l",
					logrus.FieldKeyMsg:   "m",
				},
			},
		),
	)
}
