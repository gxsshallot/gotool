package imgresize

import (
	"bytes"
	"image/png"
	"os"

	"github.com/nfnt/resize"
)

type ImageConverterPNG struct {
}

func (ImageConverterPNG) Convert(fileObj *os.File, params Params) (imageBuf bytes.Buffer, err error) {
	imgOrigin, err := png.Decode(fileObj)
	if err != nil {
		return
	}
	canvas := resize.Thumbnail(params.Width, params.Height, imgOrigin, resize.Lanczos3)
	err = png.Encode(&imageBuf, canvas)
	return
}
