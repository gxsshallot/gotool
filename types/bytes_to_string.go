package types

import (
	"bytes"
)

// 字节数组->字符串，以\0结束的字节数组，第一个0就是结束符
// 如果是定长数组，将定长数组转为切片后传入
func BytesToString(byteArr []byte) string {
	idx := bytes.IndexByte(byteArr, 0)
	if idx >= 0 {
		return string(byteArr[:idx])
	} else {
		return string(byteArr)
	}
}
