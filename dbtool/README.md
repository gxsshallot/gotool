# dbtool

这里使用GORM框架，数据库支持MySQL等，包含了一些常用的工具，包括数据库连接、分页查询列表等。

连接MySQL：

```go
import _ "github.com/go-sql-driver/mysql"
db, err := dbtool.ConnectMysql(...)
```

其他接口：

* `Transaction`：GORM事务处理。
* `IsTransaction`：判断是否是GORM事务句柄。

复杂查询条件，包括原子级条件项和复合条件项，其中：

* 原子级条件项：只有左值、右值、操作符，拼接成一个原子级查询语句。
* 复合条件项：包括多个子项，子项可以是原子级条件项，也可以是复合条件项，用连接符连接，可以是and或or。

其中`QuerySql`接口可以获取生成的查询语句和条件项，`Fields`接口可以获取查询条件中的所有字段列表，用于判断是否有字段不符合后台要求。
