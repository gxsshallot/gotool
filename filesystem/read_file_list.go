package filesystem

import (
	"io"
	"os"
)

/*
 * 对于目录中的文件数可能超过100万时，读取文件名列表的效率就至关重要了。
 * Go的读取函数中，按效率排序依次如下：
 *   os.File.Readdir
 *   ioutil.ReadDir
 *   filepath.Walk
 *   filepath.Glob
 */
func ReadFileList(
	dirPath string,
	batchCount int,
	processFile func(item os.FileInfo) (isBreak bool),
) (err error) {
	if processFile == nil {
		return
	}
	dirFile, err := os.Open(dirPath)
	if err != nil {
		return
	}
	defer dirFile.Close()
	for {
		files, e := dirFile.Readdir(batchCount)
		if e == io.EOF {
			return
		}
		if err = e; e != nil {
			return
		}
		for _, item := range files {
			isBreak := processFile(item)
			if isBreak {
				return
			}
		}
	}
}
