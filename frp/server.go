/*
 * 本地运行的frpc容器，反向代理的云端端口池管理
 * 实际访问服务，就是访问这个端口池分配的端口号
 * 包含tcp和udp服务，在程序运行之初需要按照数据库的状态初始化已使用项
 */

package frp

import (
	"strings"
	"sync"

	"github.com/fatedier/frp/pkg/auth"
	"github.com/fatedier/frp/pkg/config"
	"github.com/fatedier/frp/pkg/consts"
)

// 服务器外部端口池
var ExternalPortManager = &PortManager{
	MinPort: 0, // 必须外部更改!!!
	MaxPort: 0, // 必须外部更改!!!
	lock:    sync.RWMutex{},
	status:  map[uint16]bool{},
	pointer: 0,
}

// sse命令
type Command struct {
	Client ClientConfig  // 客户端连接配置
	Proxy  []ProxyConfig // 代理列表
}

func (cmd Command) Generate() (config.ClientCommonConf, map[string]config.ProxyConf) {
	cliConf := cmd.Client.Generate()
	proxyConfs := make(map[string]config.ProxyConf)
	for _, item := range cmd.Proxy {
		cfg := item.Generate()
		if cfg != nil {
			proxyConfs[item.Identifier] = cfg
		}
	}
	return cliConf, proxyConfs
}

// sse命令的客户端连接配置
type ClientConfig struct {
	ServerAddr  string // 服务器地址
	ServerPort  uint16 // 服务器端口号
	ServerToken string // (可选) 服务器Token鉴权
}

func (cc ClientConfig) Generate() config.ClientCommonConf {
	cliConf := config.GetDefaultClientConf()
	cliConf.ServerAddr = cc.ServerAddr
	cliConf.ServerPort = int(cc.ServerPort)
	if len(cc.ServerToken) > 0 {
		cliConf.ClientConfig = auth.GetDefaultClientConf()
		cliConf.Token = cc.ServerToken
	}
	cliConf.Complete()
	return cliConf
}

// sse命令内容的代理信息
type ProxyConfig struct {
	Identifier    string // 服务标识，全局唯一
	Mode          string // 模式，http/tcp/udp/stcp
	LocalIp       string // 本地IP
	LocalPort     uint16 // 本地端口号
	RemotePort    uint16 // 远程端口号，http模式为0
	CustomDomains string // 自定义域名，非http模式为空
	SecretKey     string // 服务密钥，仅stcp模式有效
}

func (pc ProxyConfig) Generate() config.ProxyConf {
	switch pc.Mode {
	case consts.HTTPProxy:
		cfg := &config.HTTPProxyConf{}
		cfg.ProxyType = consts.HTTPProxy
		cfg.LocalIP = pc.LocalIp
		cfg.LocalPort = int(pc.LocalPort)
		if len(pc.CustomDomains) > 0 {
			cfg.CustomDomains = strings.Split(pc.CustomDomains, ",")
		}
		cfg.UseCompression = true
		return cfg
	case consts.TCPProxy:
		cfg := &config.TCPProxyConf{}
		cfg.ProxyType = consts.TCPProxy
		cfg.LocalIP = pc.LocalIp
		cfg.LocalPort = int(pc.LocalPort)
		cfg.RemotePort = int(pc.RemotePort)
		cfg.UseCompression = true
		return cfg
	case consts.UDPProxy:
		cfg := &config.UDPProxyConf{}
		cfg.ProxyType = consts.UDPProxy
		cfg.LocalIP = pc.LocalIp
		cfg.LocalPort = int(pc.LocalPort)
		cfg.RemotePort = int(pc.RemotePort)
		cfg.UseCompression = true
		return cfg
	case consts.STCPProxy:
		cfg := &config.STCPProxyConf{}
		cfg.ProxyType = consts.STCPProxy
		cfg.UseCompression = true
		cfg.Sk = pc.SecretKey
		cfg.Plugin = "unix_domain_socket"
		cfg.PluginParams["plugin_unix_path"] = "/var/run/docker.sock"
		return cfg
	default:
		return nil
	}
}
