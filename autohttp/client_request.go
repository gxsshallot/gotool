package autohttp

import (
	"io"
	"net/http"
)

// 发送HTTP网络请求
func request(
	method string,
	url string,
	header map[string]string,
	body io.Reader,
) (statusCode int, rspBytes []byte, err error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return
	}
	for k, v := range header {
		req.Header.Set(k, v)
	}
	client := http.DefaultClient
	rsp, err := client.Do(req)
	if err != nil {
		return
	}
	statusCode = rsp.StatusCode
	rspBytes, err = io.ReadAll(rsp.Body)
	return
}
