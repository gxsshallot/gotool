/*
 * 通用的端口管理类
 * 在[最小值,最大值)之间分配端口，并记录端口使用情况
 */

package frp

import (
	"errors"
	"sync"
)

// 端口管理类
type PortManager struct {
	MinPort uint16          // (需外部设置) 最小端口号(含)
	MaxPort uint16          // (需外部设置) 最大端口号(不含)
	lock    sync.RWMutex    // 读写锁
	status  map[uint16]bool // 端口占用情况映射
	pointer uint16          // 指针增量，从0开始
}

// 从端口池取出一个端口号使用
func (pm *PortManager) Get() (port uint16, err error) {
	pm.lock.Lock()
	defer pm.lock.Unlock()
	old, cur := pm.pointer, pm.pointer
	for {
		// 计算下一个指针
		cur++
		if cur >= pm.MaxPort-pm.MinPort {
			cur = 0
		}
		if old == cur {
			err = errors.New("未找到有效端口号")
			return
		}
		port = pm.MinPort + cur
		// 判断是否已占用
		if v, ok := pm.status[port]; ok && v {
			continue
		}
		// 找到就返回
		pm.pointer = cur
		pm.status[port] = true
		return
	}
}

// 放回一个端口号
func (pm *PortManager) Put(port uint16) {
	if !pm.InRange(port) {
		return
	}
	pm.lock.Lock()
	defer pm.lock.Unlock()
	delete(pm.status, port)
}

// 初始化端口号使用情况
func (pm *PortManager) Init(statusMap map[uint16]bool) {
	pm.lock.Lock()
	defer pm.lock.Unlock()
	for k, v := range statusMap {
		if pm.InRange(k) && v {
			pm.status[k] = v
		}
	}
}

// 端口号是否被占用
func (pm *PortManager) IsUsed(port uint16) bool {
	if !pm.InRange(port) {
		return true
	}
	pm.lock.RLock()
	defer pm.lock.RUnlock()
	v, ok := pm.status[port]
	return ok && v
}

// 端口号是否有效
func (pm *PortManager) InRange(port uint16) bool {
	return port >= pm.MinPort && port < pm.MaxPort
}
