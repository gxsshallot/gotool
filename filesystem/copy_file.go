package filesystem

import (
	"io"
	"os"
)

// 拷贝文件，从source到dest。
func CopyFile(source string, dest string) (written int64, err error) {
	sourceFile, err := os.Open(source)
	if err != nil {
		return
	}
	defer sourceFile.Close()
	destFile, err := os.Create(dest)
	if err != nil {
		return
	}
	defer destFile.Close()
	written, err = io.Copy(destFile, sourceFile)
	if err != nil {
		return
	}
	_ = destFile.Sync()
	return
}
