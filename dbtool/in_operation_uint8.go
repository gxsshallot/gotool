package dbtool

import (
	"fmt"
	"strings"
)

// 由于xxx in/not in (?)时，如果值数组是uint8数组，则GORM会按照字符串处理
func InOperationUint8(
	fieldName string,
	values []uint8,
	isIn bool,
) string {
	valueStrs := make([]string, len(values))
	for i, v := range values {
		valueStrs[i] = fmt.Sprintf("%d", v)
	}
	arrStr := strings.Join(valueStrs, ",")
	if isIn {
		return fmt.Sprintf("%s in (%s)", fieldName, arrStr)
	} else {
		return fmt.Sprintf("%s not in (%s)", fieldName, arrStr)
	}
}
