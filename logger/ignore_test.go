package logger

import (
	"encoding/json"
	"testing"
)

type KV map[string]interface{}
type FD []string

func TestReplaceAndDelete(t *testing.T) {
	testList := []struct {
		Origin       KV
		IsDelete     bool
		IgnoreFields FD
		Result       KV
	}{
		{KV{"a": 0}, true, FD{}, KV{"a": 0}},
		{KV{"b": "1"}, true, FD{"b"}, KV{}},
		{KV{"b": 1}, false, FD{"b"}, KV{"b": "-"}},
		{KV{"b": 1}, true, FD{"c"}, KV{"b": 1}},
		{KV{"c": KV{"c1": "21", "c2": 22}}, true, FD{"c.c1"}, KV{"c": KV{"c2": 22}}},
		{KV{"c": KV{"c1": 21, "c2": 22}}, false, FD{"c.c1"}, KV{"c": KV{"c1": "-", "c2": 22}}},
		{KV{"c": KV{"c1": 21, "c2": 22}}, true, FD{"c.c3"}, KV{"c": KV{"c1": 21, "c2": 22}}},
		{KV{"d": "{\"d1\":\"31\",\"d2\":32}"}, true, FD{"d~.d1"}, KV{"d": "{\"d2\":32}"}},
		{KV{"d": "{\"d1\":31,\"d2\":32}"}, false, FD{"d~.d1"}, KV{"d": "{\"d1\":\"-\",\"d2\":32}"}},
		{KV{"d": "{\"d1\":31,\"d2\":32}"}, true, FD{"d~.d3"}, KV{"d": "{\"d1\":31,\"d2\":32}"}},
		{KV{"e": []KV{{"e1": "41"}, {"e2": 43}}}, true, FD{"e*.e1"}, KV{"e": []KV{{}, {"e2": 43}}}},
		{KV{"e": []KV{{"e1": 41}, {"e2": 43}}}, false, FD{"e*.e1"}, KV{"e": []KV{{"e1": "-"}, {"e2": 43}}}},
		{KV{"e": []KV{{"e1": 41}, {"e2": 43}}}, true, FD{"e*.e3"}, KV{"e": []KV{{"e1": 41}, {"e2": 43}}}},
	}
	var output string
	for _, item := range testList {
		if item.IsDelete {
			output = DeleteObject(item.Origin, item.IgnoreFields)
		} else {
			output = ReplaceObject(item.Origin, "-", item.IgnoreFields)
		}
		resultStr, _ := json.Marshal(item.Result)
		if output != string(resultStr) {
			t.Errorf("invalid result\nresult: %s\noutput: %s", string(resultStr), output)
		}
	}
}
