package gomicro

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	consul "github.com/hashicorp/consul/api"
	hash "github.com/mitchellh/hashstructure"
)

type ConsulRegistry struct {
	Address     []string       // 服务地址
	opts        ConsulOptions  // 配置项
	client      *consul.Client // API的连接
	register    map[string]uint64
	lastChecked map[string]time.Time // 记录节点最后检查存在的时间
}

func NewConsulRegistry(opts ConsulOptions) *ConsulRegistry {
	c := &ConsulRegistry{
		opts:        opts,
		register:    make(map[string]uint64),
		lastChecked: make(map[string]time.Time),
	}
	// use default non pooled config
	if c.opts.ConsulConfig == nil {
		c.opts.ConsulConfig = consul.DefaultNonPooledConfig()
	}
	if c.opts.QueryOptions == nil {
		c.opts.QueryOptions = &consul.QueryOptions{}
	}
	c.opts.QueryOptions.AllowStale = !c.opts.DenyStale
	// iterate the options addresses
	for _, address := range c.opts.Addrs {
		// check we have a port
		addr, port, err := net.SplitHostPort(address)
		if ae, ok := err.(*net.AddrError); ok && ae.Err == "missing port in address" {
			c.Address = append(c.Address, net.JoinHostPort(address, "8500"))
		} else if err == nil {
			c.Address = append(c.Address, net.JoinHostPort(addr, port))
		}
	}
	// set the addrs
	if len(c.Address) > 0 {
		c.opts.ConsulConfig.Address = c.Address[0]
	}
	if c.opts.ConsulConfig.HttpClient == nil {
		c.opts.ConsulConfig.HttpClient = new(http.Client)
	}
	if c.opts.Timeout > 0 {
		c.opts.ConsulConfig.HttpClient.Timeout = c.opts.Timeout
	}
	// remove and setup the client
	c.client = nil
	c.Client()
	return c
}

func getDeregisterTTL(t time.Duration) time.Duration {
	// splay slightly for the watcher?
	splay := time.Second * 5
	deregTTL := t + splay
	// consul has a minimum timeout on deregistration of 1 minute.
	if t < time.Minute {
		deregTTL = time.Minute + splay
	}
	return deregTTL
}

func (c *ConsulRegistry) Deregister(s *RegistryService) error {
	if len(s.Nodes) == 0 {
		return errors.New("require at least one node")
	}
	// delete our hash and time check of the service
	delete(c.register, s.Name)
	delete(c.lastChecked, s.Name)
	node := s.Nodes[0]
	return c.Client().Agent().ServiceDeregister(node.Id)
}

func (c *ConsulRegistry) Register(s *RegistryService, ttl time.Duration) error {
	if len(s.Nodes) == 0 {
		return errors.New("require at least one node")
	}
	var regTCPCheck bool
	var regInterval time.Duration
	var regHTTPCheck bool
	var httpCheckConfig consul.AgentServiceCheck
	if c.opts.TCPCheck > 0 {
		regTCPCheck = true
		regInterval = c.opts.TCPCheck
	}
	if c.opts.HTTPCheck != nil {
		httpCheckConfig = *c.opts.HTTPCheck
		regHTTPCheck = true
	}
	// create hash of service; uint64
	h, err := hash.Hash(s, nil)
	if err != nil {
		return err
	}
	// use first node
	node := s.Nodes[0]
	// get existing hash and last checked time
	v, ok := c.register[s.Name]
	lastChecked := c.lastChecked[s.Name]
	// if it's already registered and matches then just pass the check
	if ok && v == h {
		if ttl == time.Duration(0) {
			// ensure that our service hasn't been deregistered by Consul
			if time.Since(lastChecked) <= getDeregisterTTL(regInterval) {
				return nil
			}
			services, _, err := c.Client().Health().Checks(s.Name, c.opts.QueryOptions)
			if err == nil {
				for _, v := range services {
					if v.ServiceID == node.Id {
						return nil
					}
				}
			}
		} else {
			// if the err is nil we're all good, bail out
			// if not, we don't know what the state is, so full re-register
			if err := c.Client().Agent().PassTTL("service:"+node.Id, ""); err == nil {
				return nil
			}
		}
	}
	// encode the tags
	tags := encodeMetadata(node.Metadata)
	tags = append(tags, encodeEndpoints(s.Endpoints)...)
	tags = append(tags, encodeVersion(s.Version)...)
	var check *consul.AgentServiceCheck
	if regTCPCheck {
		deregTTL := getDeregisterTTL(regInterval)
		check = &consul.AgentServiceCheck{
			TCP:                            node.Address,
			Interval:                       fmt.Sprintf("%v", regInterval),
			DeregisterCriticalServiceAfter: fmt.Sprintf("%v", deregTTL),
		}
	} else if regHTTPCheck {
		interval, _ := time.ParseDuration(httpCheckConfig.Interval)
		deregTTL := getDeregisterTTL(interval)
		host, _, _ := net.SplitHostPort(node.Address)
		healthCheckURI := strings.Replace(httpCheckConfig.HTTP, "{host}", host, 1)
		check = &consul.AgentServiceCheck{
			HTTP:                           healthCheckURI,
			Interval:                       httpCheckConfig.Interval,
			Timeout:                        httpCheckConfig.Timeout,
			DeregisterCriticalServiceAfter: fmt.Sprintf("%v", deregTTL),
		}
		// if the TTL is greater than 0 create an associated check
	} else if ttl > time.Duration(0) {
		deregTTL := getDeregisterTTL(ttl)
		check = &consul.AgentServiceCheck{
			TTL:                            fmt.Sprintf("%v", ttl),
			DeregisterCriticalServiceAfter: fmt.Sprintf("%v", deregTTL),
		}
	}
	host, pt, _ := net.SplitHostPort(node.Address)
	if host == "" {
		host = node.Address
	}
	port, _ := strconv.Atoi(pt)
	// register the service
	asr := &consul.AgentServiceRegistration{
		ID:      node.Id,
		Name:    s.Name,
		Tags:    tags,
		Port:    port,
		Address: host,
		Meta:    node.Metadata,
		Check:   check,
	}
	// Specify consul connect
	if c.opts.Connect {
		asr.Connect = &consul.AgentServiceConnect{
			Native: true,
		}
	}
	if err := c.Client().Agent().ServiceRegister(asr); err != nil {
		return err
	}
	// save our hash and time check of the service
	c.register[s.Name] = h
	c.lastChecked[s.Name] = time.Now()
	// if the TTL is 0 we don't mess with the checks
	if ttl == time.Duration(0) {
		return nil
	}
	// pass the healthcheck
	return c.Client().Agent().PassTTL("service:"+node.Id, "")
}

func (c *ConsulRegistry) GetService(name string) ([]*RegistryService, error) {
	var rsp []*consul.ServiceEntry
	var err error
	// if we're connect enabled only get connect services
	if c.opts.Connect {
		rsp, _, err = c.Client().Health().Connect(name, "", false, c.opts.QueryOptions)
	} else {
		rsp, _, err = c.Client().Health().Service(name, "", false, c.opts.QueryOptions)
	}
	if err != nil {
		return nil, err
	}
	serviceMap := map[string]*RegistryService{}
	for _, s := range rsp {
		if s.Service.Service != name {
			continue
		}
		// version is now a tag
		version, _ := decodeVersion(s.Service.Tags)
		// service ID is now the node id
		id := s.Service.ID
		// key is always the version
		key := version
		// address is service address
		address := s.Service.Address
		// use node address
		if len(address) == 0 {
			address = s.Node.Address
		}
		svc, ok := serviceMap[key]
		if !ok {
			svc = &RegistryService{
				Endpoints: decodeEndpoints(s.Service.Tags),
				Name:      s.Service.Service,
				Version:   version,
			}
			serviceMap[key] = svc
		}
		var del bool
		for _, check := range s.Checks {
			// delete the node if the status is critical
			if check.Status == "critical" {
				del = true
				break
			}
		}
		// if delete then skip the node
		if del {
			continue
		}
		svc.Nodes = append(svc.Nodes, &RegistryNode{
			Id:       id,
			Address:  HostPort(address, s.Service.Port),
			Metadata: decodeMetadata(s.Service.Tags),
		})
	}
	var services []*RegistryService
	for _, service := range serviceMap {
		services = append(services, service)
	}
	return services, nil
}

func (c *ConsulRegistry) String() string {
	return "consul"
}

// 获取API连接，优先返回已有的连接
func (c *ConsulRegistry) Client() *consul.Client {
	if c.client != nil {
		return c.client
	}
	for _, addr := range c.Address {
		// set the address
		c.opts.ConsulConfig.Address = addr
		// create a new client
		tmpClient, _ := consul.NewClient(c.opts.ConsulConfig)
		// test the client
		_, err := tmpClient.Agent().Host()
		if err != nil {
			continue
		}
		// set the client
		c.client = tmpClient
		return c.client
	}
	// set the default
	c.client, _ = consul.NewClient(c.opts.ConsulConfig)
	// return the client
	return c.client
}
