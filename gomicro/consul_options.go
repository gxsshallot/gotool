package gomicro

import (
	"time"

	consul "github.com/hashicorp/consul/api"
)

type ConsulOptions struct {
	Addrs        []string             // 服务地址
	Timeout      time.Duration        // 超时时间
	Connect      bool                 // Connect specifies services should be registered as Consul Connect services.
	ConsulConfig *consul.Config       // API的配置信息
	DenyStale    bool                 // AllowStale sets whether any Consul server (non-leader) can service a read. This allows for lower latency and higher throughput at the cost of potentially stale data. Works similar to Consul DNS Config option [1]. Defaults to true. [1] https://www.consul.io/docs/agent/options.html#allow_stale
	QueryOptions *consul.QueryOptions // QueryOptions specifies the QueryOptions to be used when calling Consul. See `Consul API` for more information [1]. [1] https://godoc.org/github.com/hashicorp/consul/api#QueryOptions
	TCPCheck     time.Duration        // TCPCheck will tell the service provider to check the service address and port every `t` interval. It will enabled only if `t` is greater than 0. See `TCP + Interval` for more information [1]. [1] https://www.consul.io/docs/agent/checks.html
	/*
		=> protocol, port, httpEndpoint string, interval, timeout time.Duration
		consul.AgentServiceCheck{
			HTTP:     fmt.Sprintf("%s://{host}:%s%s", protocol, port, httpEndpoint),
			Interval: fmt.Sprintf("%v", interval),
			Timeout:  fmt.Sprintf("%v", timeout),
		}
	*/
	HTTPCheck *consul.AgentServiceCheck // HTTPCheck will tell the service provider to invoke the health check endpoint with an interval and timeout. It will be enabled only if interval and timeout are greater than 0. See `HTTP + Interval` for more information [1]. [1] https://www.consul.io/docs/agent/checks.html
}
