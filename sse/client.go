package sse

import (
	"github.com/r3labs/sse/v2"
)

// 客户端
type Client struct {
	Instance *sse.Client // 连接
	Url      string      // 服务端地址
}

// 新建客户端
func NewClient(url string) *Client {
	c := new(Client)
	c.Url = url
	c.Instance = sse.NewClient(url)
	return c
}

// 订阅信息流
func (c *Client) Subscribe(stream string, f func(data []byte)) error {
	return c.Instance.Subscribe(stream, func(msg *sse.Event) {
		if f != nil {
			f(msg.Data)
		}
	})
}
