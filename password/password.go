package password

import "golang.org/x/crypto/bcrypt"

func Generate(userPassword string) (str string, err error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(userPassword), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	str = string(bytes)
	return
}

func Validate(hashedPassword string, userPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(userPassword))
}
