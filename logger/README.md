# logger

业务日志记录模块。

首先要调用初始化函数：

```go
// 参数是一个目录路径，如果为空，则直接输出到标准输出上
logger.Init("[LOG_PATH_DIR]")
```

然后在`gin`的中间件中，第一个位置插入`logger.Middleware()`中间件，用于自动记录日志，并且在每一个请求的`gin.Context`中，生成一个唯一的标识ID。

在内部业务逻辑中，需要调用如下方法进行日志记录：

```go
logger.Info(ctx, "记录正常信息")
logger.Info(ctx, "记录正常信息 %s %d", "HelloWorld!", 100)
logger.Error(ctx, err, "记录错误")
logger.Fatal(ctx, err, "记录崩溃")
```

这样会自动将`context`中的标识ID记录到日志中。

注：`github.com/lestrrat-go/file-rotatelogs`库，请使用最新的`commit`号作为版本。

### Ignore模块

`ignore.go`是用来忽略Json序列化结果的一些字段，主要用于日志记录时，某些Json结果中包含非常长的字符串或需要保密的隐私信息等，这些需要被忽略输出。

接口：

* `ReplaceObject`：替换通用对象指定字段，必须是可以Json序列化的对象。
* `ReplaceString`：替换Json字符串指定字段。
* `DeleteObject`：删除通用对象指定字段，必须是可以Json序列化的对象。
* `DeleteString`：删除Json字符串指定字段。

其中替换和删除的忽略字段格式如下，参照[测试样例](ignore_test.go)：

* "k1": 表示`data["k1"]`
* "k1.k2": 表示`data["k1"]["k2"]`
* "k1~.k2": "k1"对应一个Json字符串，解码后删除"k2"字段
* "k1*.k2": "k1"是一个数组，在数组的每个元素对象中删除"k2"字段

样例：

```go
// input = {"a":1}
// result = {"a":"-"}
result := jsonutil.ReplaceObject(input, "-", []string{"a"})

// input = {"a":{"a1": 1}}
// result = {"a":{}}
result := jsonutil.DeleteObject(input, []string{"a.a1"})

// input = {"a":"{\"a1\":1}"}
// result = {"a":"{\"a1\":\"-\"}"}
result := jsonutil.ReplaceObject(input, "-", []string{"a~.a1"})

// input = {"a":[{"a1":1},{"a2":2}]}
// result = {"a":[{},{"a2":2}]}
result := jsonutil.DeleteObject(input, []string{"a*.a1"})
```
