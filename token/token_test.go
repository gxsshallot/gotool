package token

import (
	"testing"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type CustomClaims struct {
	jwt.StandardClaims
	UserId     uint64 `json:"uid"` // 员工ID
	Env        string `json:"ev"`  // 环境信息
	ExpireTime int64  `json:"et"`  // 有效截止时间的时间戳
}

func TestToken(t *testing.T) {
	claims := CustomClaims{
		UserId:     1,
		Env:        "web",
		ExpireTime: time.Now().Unix(),
	}
	token, err := GenerateToken[*CustomClaims](&claims, "1234567890")
	if err != nil {
		t.Error(err)
	} else {
		t.Log(token)
	}
	var gotClaims CustomClaims
	err = ParseToken[*CustomClaims](token, "1234567890", &gotClaims)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(gotClaims)
	}
}
