package logger

import (
	"context"
	"sync"
	"time"
)

var (
	Interval = int64(60) // 输出间隔，单位为秒，默认间隔60秒输出
)

var (
	loggerIntervalMap     = map[string]int64{}
	loggerIntervalMapLock = sync.RWMutex{}
)

func LoopInfo(c context.Context, logId string, info string, fields ...interface{}) {
	if needLog(logId) {
		Info(c, info, fields...)
	}
}

func LoopError(c context.Context, logId string, err error, info string, fields ...interface{}) {
	if needLog(logId) {
		Error(c, err, info, fields...)
	}
}

func LoopFatal(c context.Context, logId string, err error, info string, fields ...interface{}) {
	if needLog(logId) {
		Fatal(c, err, info, fields...)
	}
}

// 判断是否应该输出
func needLog(logId string) bool {
	now := time.Now().Unix()
	if lastTimestamp, ok := getMapValue(logId); ok {
		if now-lastTimestamp > Interval {
			setMapValue(logId, now)
			return true
		} else {
			return false
		}
	} else {
		setMapValue(logId, now)
		return true
	}
}

func getMapValue(logId string) (int64, bool) {
	loggerIntervalMapLock.RLock()
	defer loggerIntervalMapLock.RUnlock()
	value, ok := loggerIntervalMap[logId]
	return value, ok
}

func setMapValue(logId string, value int64) {
	loggerIntervalMapLock.Lock()
	defer loggerIntervalMapLock.Unlock()
	loggerIntervalMap[logId] = value
}

func RemoveLogId(logId string) {
	loggerIntervalMapLock.Lock()
	defer loggerIntervalMapLock.Unlock()
	delete(loggerIntervalMap, logId)
}
