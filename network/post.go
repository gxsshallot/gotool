package network

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

/*
发送内部请求(POST)，并解析返回值。

	baseUrl：基地址
	apiUrl：URL后面的相对地址
	headers：HTTP头信息，大小写敏感
	requestBody：请求的Body
	hasResponse：是否解析应答
*/
func Post[R any](
	baseUrl string,
	apiUrl string,
	headers map[string]interface{},
	requestBody interface{},
	hasResponse bool,
) (rsp R, err error) {
	// 封装Body
	bodyData, err := json.Marshal(requestBody)
	if err != nil {
		return
	}
	bodyReader := bytes.NewReader(bodyData)
	// 生成URL
	baseUrl = strings.TrimSuffix(baseUrl, "/")
	apiUrl = strings.TrimPrefix(apiUrl, "/")
	fullUrl := fmt.Sprintf("%s/%s", baseUrl, apiUrl)
	// 生成POST请求
	request, err := http.NewRequest("POST", fullUrl, bodyReader)
	if err != nil {
		return
	}
	// 设置Header
	request.Header["Content-Type"] = []string{"application/json;charset=UTF-8"}
	for k, v := range headers {
		request.Header[k] = []string{fmt.Sprintf("%v", v)}
	}
	// 生成Client并发送请求
	client := http.Client{}
	rspHttp, err := client.Do(request)
	if err != nil {
		return
	}
	defer rspHttp.Body.Close()
	// 判断HTTP返回值
	if rspHttp.StatusCode != http.StatusOK {
		err = errors.New(rspHttp.Status)
		return
	}
	// 读取返回值Body
	rspBytes, err := io.ReadAll(rspHttp.Body)
	if err != nil {
		return
	}
	// 解析返回值
	if !hasResponse {
		return
	}
	if err = jsonUnmarshal(rspBytes, &rsp); err != nil {
		return
	}
	return
}
