package logger

import (
	"encoding/json"
	"strings"
)

// 处理Json字符串
func processJsonString(
	jsonStr []byte,
	isDelete bool,
	replaceStr string,
	fields []string,
) []byte {
	// 没有字段，直接返回
	if len(fields) == 0 {
		return jsonStr
	}
	// 反序列化错误，直接返回
	var data map[string]interface{}
	if err := json.Unmarshal(jsonStr, &data); err != nil {
		return jsonStr
	}
	// 处理字段
	for _, ignoreField := range fields {
		fields := strings.Split(ignoreField, ".")
		processField(data, isDelete, replaceStr, fields)
	}
	// 序列化后返回
	if str, err := json.Marshal(data); err != nil {
		return jsonStr
	} else {
		return str
	}
}

// 递归处理Json对象的字段
// isDelete=true，表示删除指定字段
// isDelete=false，表示替换指定字段为replaceStr
// splittedField，是字段用'.'拆分后的结果
func processField(
	data map[string]interface{},
	isDelete bool,
	replaceStr string,
	splittedField []string,
) {
	// 没有字段要处理
	if len(splittedField) == 0 {
		return
	}
	// 取出第一个字段处理
	cur := splittedField[0]
	if len(cur) == 0 {
		return
	}
	isString := cur[len(cur)-1] == '~'
	isArray := cur[len(cur)-1] == '*'
	if isString || isArray {
		cur = cur[:len(cur)-1]
	}
	v, ok := data[cur]
	if !ok {
		return
	}
	// 如果只剩一层了，就直接删除或替换
	if len(splittedField) == 1 {
		if isDelete {
			delete(data, cur)
		} else {
			data[cur] = replaceStr
		}
		return
	}
	// 递归处理所有字段的值
	switch v := v.(type) {
	case map[string]interface{}:
		processField(v, isDelete, replaceStr, splittedField[1:])
	case []interface{}:
		if isArray {
			for _, vItem := range v {
				switch vItem := vItem.(type) {
				case map[string]interface{}:
					processField(vItem, isDelete, replaceStr, splittedField[1:])
				default:
					continue
				}
			}
		}
	case string:
		if isString {
			var innerData map[string]interface{}
			if e := json.Unmarshal([]byte(v), &innerData); e != nil {
				return
			}
			processField(innerData, isDelete, replaceStr, splittedField[1:])
			if newStr, e := json.Marshal(innerData); e == nil {
				data[cur] = string(newStr)
			}
		}
	}
}
