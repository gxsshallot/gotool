package autohttp

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
)

// 默认实现的表名称
var DefaultImplementTableName = "autohttp"

// 默认实现的表结构
type DefaultImplementTable struct {
	ID                  uint64 `gorm:"auto_increment:true;primary_key"` // 唯一标识ID
	BusinessType        string `gorm:"size:255;default:''"`             // 业务类型，用户自定义
	BusinessId          string `gorm:"size:255;default:''"`             // 业务主键，同一业务类型不能出现重复
	Url                 string `gorm:"size:255;default:''"`             // HTTP请求URL
	Method              string `gorm:"size:20;default:''"`              // HTTP请求方法
	Header              string `gorm:"type:text"`                       // HTTP请求头
	Body                string `gorm:"type:text"`                       // HTTP请求Body
	RetryType           bool   `gorm:"default:0"`                       // 重试类型
	RetryLadderInterval string `gorm:"size:255;default:''"`             // 阶梯性重试的间隔，使用逗号分隔
	RetryStepInterval   int64  `gorm:"default:0"`                       // 周期性重试的间隔
	RetryStepTime       int64  `gorm:"default:0"`                       // 周期性重试的次数
	RetryCount          int64  `gorm:"default:0"`                       // 重试计数
	Status              uint8  `gorm:"default:0"`                       // 状态，参见常量StatusXXX
	RequestTime         int64  `gorm:"default:0"`                       // 首次发起请求时间，Unix时间戳，单位秒
	LastRetryTime       int64  `gorm:"default:0"`                       // 上一次重试的时间，Unix时间戳，单位秒
	NextRetryTime       int64  `gorm:"default:0"`                       // 下一次重试时间，Unix时间戳，单位秒
	SuccessTime         int64  `gorm:"default:0"`                       // 成功时间，Unix时间戳，单位秒
}

// 默认实现对象
type DefaultImplement struct {
	db *gorm.DB
}

// 初始化结构体
func (impl *DefaultImplement) SetDB(db *gorm.DB) (err error) {
	if db == nil {
		err = errors.New("不能设置空的数据库句柄")
		return
	}
	impl.db = db
	return
}

// 创建初始任务
func (impl DefaultImplement) CreateTask(c context.Context, item *Item) (err error) {
	dbItem, err := impl.convertCommonItem(*item)
	if err != nil {
		return
	}
	if err = impl.db.Table(DefaultImplementTableName).Create(&dbItem).Error; err != nil {
		return
	}
	item.ID = dbItem.ID
	return
}

// 删除任务
func (impl DefaultImplement) DeleteTask(c context.Context, taskId uint64) (err error) {
	err = impl.db.Table(DefaultImplementTableName).Delete(DefaultImplementTable{}, "id = ?", taskId).Error
	return
}

// 查询待重试的任务(仅ID)
func (impl DefaultImplement) IncompleteTaskIdList(c context.Context, currentTime int64) (list []uint64, err error) {
	db := impl.db.Table(DefaultImplementTableName)
	db = db.Where("status = ?", StatusFailure)
	db = db.Where("next_retry_time < ?", currentTime)
	err = db.Pluck("id", &list).Error
	return
}

// 待清理任务列表，status表示状态，beforeSecond表示多少秒之前(使用RequestTime计算)的任务
func (impl DefaultImplement) TaskToCleanList(c context.Context, status uint8, timestamp int64) (list []uint64, err error) {
	db := impl.db.Table(DefaultImplementTableName)
	db = db.Where("status = ?", status)
	db = db.Where("last_retry_time < ?", timestamp)
	err = db.Pluck("id", &list).Error
	return
}

// 查询任务详细信息
func (impl DefaultImplement) TaskDetail(c context.Context, taskId uint64) (item Item, err error) {
	var dbItem DefaultImplementTable
	if err = impl.db.Table(DefaultImplementTableName).First(&dbItem, "id = ?", taskId).Error; err != nil {
		return
	}
	item = impl.convertDbItem(dbItem)
	return
}

// 查询任务详细信息
func (impl DefaultImplement) TaskDetailByBusiness(c context.Context, businessType string, businessId string) (item Item, err error) {
	var dbItem DefaultImplementTable
	db := impl.db.Table(DefaultImplementTableName)
	db = db.Where("business_type = ?", businessType)
	db = db.Where("business_id = ?", businessId)
	if err = db.First(&dbItem).Error; err != nil {
		return
	}
	item = impl.convertDbItem(dbItem)
	return
}

// 更新任务
func (impl DefaultImplement) UpdateTask(c context.Context, item *Item) (err error) {
	dbItem, err := impl.convertCommonItem(*item)
	if err != nil {
		return
	}
	err = impl.db.Table(DefaultImplementTableName).Save(&dbItem).Error
	return
}

func (impl DefaultImplement) convertCommonItem(item Item) (dbItem DefaultImplementTable, err error) {
	headerStr, err := json.Marshal(item.Header)
	if err != nil {
		return
	}
	ladderStrArray := []string{}
	for _, ladderItem := range item.RetryLadderInterval {
		ladderStrArray = append(ladderStrArray, fmt.Sprintf("%d", ladderItem))
	}
	dbItem = DefaultImplementTable{
		ID:                  item.ID,
		BusinessType:        item.BusinessType,
		BusinessId:          item.BusinessId,
		Url:                 item.Url,
		Method:              item.Method,
		Header:              string(headerStr),
		Body:                string(item.Body),
		RetryType:           item.RetryType,
		RetryLadderInterval: strings.Join(ladderStrArray, ","),
		RetryStepInterval:   item.RetryStepInterval,
		RetryStepTime:       item.RetryStepTime,
		RetryCount:          item.RetryCount,
		Status:              item.Status,
		RequestTime:         item.RequestTime,
		LastRetryTime:       item.LastRetryTime,
		NextRetryTime:       item.NextRetryTime,
		SuccessTime:         item.SuccessTime,
	}
	return
}

func (impl DefaultImplement) convertDbItem(dbItem DefaultImplementTable) Item {
	var header map[string]string
	_ = json.Unmarshal([]byte(dbItem.Header), &header)
	ladderStrArray := strings.Split(dbItem.RetryLadderInterval, ",")
	ladder := []int64{}
	for _, ladderStr := range ladderStrArray {
		ladderItem, e := strconv.ParseInt(ladderStr, 10, 64)
		if e == nil {
			ladder = append(ladder, ladderItem)
		}
	}
	return Item{
		ID:                  dbItem.ID,
		BusinessType:        dbItem.BusinessType,
		BusinessId:          dbItem.BusinessId,
		Url:                 dbItem.Url,
		Method:              dbItem.Method,
		Header:              header,
		Body:                []byte(dbItem.Body),
		RetryType:           dbItem.RetryType,
		RetryLadderInterval: ladder,
		RetryStepInterval:   dbItem.RetryStepInterval,
		RetryStepTime:       dbItem.RetryStepTime,
		RetryCount:          dbItem.RetryCount,
		Status:              dbItem.Status,
		RequestTime:         dbItem.RequestTime,
		LastRetryTime:       dbItem.LastRetryTime,
		NextRetryTime:       dbItem.NextRetryTime,
		SuccessTime:         dbItem.SuccessTime,
	}
}
