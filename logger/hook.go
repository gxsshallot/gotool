package logger

import (
	"github.com/sirupsen/logrus"
)

type ContextHook struct{}

func (hook *ContextHook) Fire(entry *logrus.Entry) error {
	trackId := GetTrackId(entry.Context)
	if len(trackId) > 0 {
		entry.Data["c"] = trackId
	}
	return nil
}

func (hook *ContextHook) Levels() []logrus.Level {
	return logrus.AllLevels
}
