package imgresize

import (
	"bytes"
	"os"
)

// 压缩参数
type Params struct {
	Width   uint // 宽度，如果为0，则表示是等比例缩放
	Height  uint // 高度，如果为0，则表示是等比例缩放
	Quality int  // 质量，0-100
}

// 通用方法
type Converter interface {
	// 转换
	Convert(fileObj *os.File, params Params) (imageBuf bytes.Buffer, err error)
}
