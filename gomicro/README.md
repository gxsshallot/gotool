# gomicro

基于[micro/go-micro](https://github.com/micro/go-micro)的`v5.3.0`版本改写的，[consul](https://github.com/micro/plugins)基于`v5/registry/consul/v1.0.2`改写的，目录为`plugins/v5/registry/consul/`。

依赖要求：`github.com/hashicorp/consul/api`原始为`v1.9.0`版本。
