# transfer

传输层协议库。

* `ReadSocket`：从Socket中读取指定长度数据。
* `ReadSocketWithMaxLength`：从Socket中读取指定长度的数据，校验最大长度是否有效。
* `IsLAN`：判断是否是局域网IP地址。
* `CheckIpRange`：判断IP是否位于区间内。
* 字节流大端序：`ReadBigEndian`、`WriteBigEndian`
* 字节流小端序：`ReadLittleEndian`、`WriteLittleEndian`
