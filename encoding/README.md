# encoding

编码工具。

* GB2312和UTF-8的转换：`Utf8ToGb2312`、`Gb2312ToUtf8`
* GBK和UTF-8的转换：`Utf8ToGbk`、`GbkToUtf8`
