package encrypt

import "crypto/cipher"

// ECB算法实现
type ECB struct {
	b         cipher.Block
	blockSize int
}

func NewECB(b cipher.Block) *ECB {
	return &ECB{
		b:         b,
		blockSize: b.BlockSize(),
	}
}

// ECB加密部分
type EcbEncrypter ECB

func NewECBEncrypter(b cipher.Block) cipher.BlockMode {
	return (*EcbEncrypter)(NewECB(b))
}

func (x *EcbEncrypter) BlockSize() int {
	return x.blockSize
}

func (x *EcbEncrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Encrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}

// ECB解密部分
type EcbDecrypter ECB

func NewECBDecrypter(b cipher.Block) cipher.BlockMode {
	return (*EcbDecrypter)(NewECB(b))
}

func (x *EcbDecrypter) BlockSize() int {
	return x.blockSize
}

func (x *EcbDecrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Decrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}
