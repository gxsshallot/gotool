package logger

import (
	"bytes"
	"fmt"
	"io"
	"strings"

	"gitee.com/gxsshallot/gotool/gin"
)

// 在路由的设置的第一个中间件
func Middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 生成TrackId
		trackId := c.GetHeader(KEY_ID)
		if trackId == "" {
			c.Set(KEY_ID, NewTrackId())
		} else {
			c.Set(KEY_ID, trackId)
		}
		// 记录请求信息
		var reqStr string
		if c.Request.Method == "POST" {
			switch c.ContentType() {
			case "multipart/form-data":
				reqStr = outputReqMpForm(c)
			default:
				reqStr = outputReqJson(c)
			}
		} else {
			reqStr = outputReqNoBody(c)
		}
		if len(reqStr) > 0 {
			Info(c, reqStr)
		}
		// 替换Writer
		writer := ResponseBodyWriter{
			ResponseWriter: c.Writer,
			Body:           new(bytes.Buffer),
		}
		c.Writer = &writer
		// 继续流程
		c.Next()
		// 记录应答信息
		contentType := c.Writer.Header().Get("Content-Type")
		var rspStr string
		if strings.Contains(contentType, "application/json") {
			rspStr = outputRspJson(c)
		} else {
			rspStr = outputRspUnknown(c)
		}
		if len(rspStr) > 0 {
			Info(c, rspStr)
		}
	}
}

// 输出Content-Type=application/json的请求
func outputReqJson(c *gin.Context) (str string) {
	// 获取路由配置
	method := strings.ToUpper(c.Request.Method)
	urlFullPath := c.Request.URL.String()
	urlPath := strings.Split(urlFullPath, "?")[0]
	conf := GetRouterConfig(method, urlPath)
	if conf.DisableReq {
		return
	}
	// 输出请求类型和URL信息
	result := []string{
		method,
		fmt.Sprintf("url=%s", urlFullPath),
	}
	// 输出Header信息
	for _, k := range conf.HeaderFields {
		v := c.GetHeader(k)
		if len(v) > 0 {
			result = append(result, fmt.Sprintf("%s=%s", k, v))
		}
	}
	// 输出Body信息
	rawData, _ := c.GetRawData()
	if conf.OutputReqBody != nil {
		result = append(result, fmt.Sprintf("body=%s", conf.OutputReqBody(c, string(rawData))))
	} else {
		outputJsonStr := ReplaceString(rawData, IgnoreReplaceStr, conf.IgnoreReqBodyFields)
		result = append(result, fmt.Sprintf("body=%s", string(outputJsonStr)))
	}
	// 还原Body
	str = strings.Join(result, " ")
	c.Request.Body = io.NopCloser(bytes.NewBuffer(rawData))
	return
}

// 输出Content-Type=multipart/form-data的请求
func outputReqMpForm(c *gin.Context) (str string) {
	// 获取路由配置
	method := strings.ToUpper(c.Request.Method)
	urlFullPath := c.Request.URL.String()
	urlPath := strings.Split(urlFullPath, "?")[0]
	conf := GetRouterConfig(method, urlPath)
	if conf.DisableReq {
		return
	}
	// 输出请求类型和URL信息
	result := []string{
		method,
		fmt.Sprintf("url=%s", urlFullPath),
	}
	// 输出Header信息
	for _, k := range conf.HeaderFields {
		v := c.GetHeader(k)
		if len(v) > 0 {
			result = append(result, fmt.Sprintf("%s=%s", k, v))
		}
	}
	// 输出信息
	rawData := c.PostForm("params")
	if conf.OutputReqBody != nil {
		result = append(result, fmt.Sprintf("body=%s", conf.OutputReqBody(c, string(rawData))))
	} else {
		outputJsonStr := ReplaceString([]byte(rawData), IgnoreReplaceStr, conf.IgnoreReqBodyFields)
		result = append(result, fmt.Sprintf("body=%s", string(outputJsonStr)))
		if fileHeader, e := c.FormFile("file"); e == nil {
			result = append(result, fmt.Sprintf("file_len=%d", fileHeader.Size))
		}
	}
	// 返回值
	str = strings.Join(result, " ")
	return
}

// 输出没有Body的请求
func outputReqNoBody(c *gin.Context) (str string) {
	// 获取路由配置
	method := strings.ToUpper(c.Request.Method)
	urlFullPath := c.Request.URL.String()
	urlPath := strings.Split(urlFullPath, "?")[0]
	conf := GetRouterConfig(method, urlPath)
	if conf.DisableReq {
		return
	}
	// 输出请求类型和URL信息
	result := []string{
		method,
		fmt.Sprintf("url=%s", urlFullPath),
	}
	// 输出Header信息
	for _, k := range conf.HeaderFields {
		v := c.GetHeader(k)
		if len(v) > 0 {
			result = append(result, fmt.Sprintf("%s=%s", k, v))
		}
	}
	// 返回结果
	str = strings.Join(result, " ")
	return
}

// 记录Content-Type=application/json应答
func outputRspJson(c *gin.Context) (str string) {
	writer := c.Writer.(*ResponseBodyWriter)
	method := strings.ToUpper(c.Request.Method)
	urlFullPath := c.Request.URL.String()
	urlPath := strings.Split(urlFullPath, "?")[0]
	conf := GetRouterConfig(method, urlPath)
	if conf.DisableRsp {
		return
	}
	result := []string{
		method + "应答",
		fmt.Sprintf("url=%s", urlFullPath),
		fmt.Sprintf("status=%d", writer.Status()),
	}
	if conf.OutputRspBody != nil {
		result = append(result, conf.OutputRspBody(c, writer.Body.String()))
	} else {
		outputJsonStr := ReplaceString(writer.Body.Bytes(), IgnoreReplaceStr, conf.IgnoreRspFields)
		result = append(result, string(outputJsonStr))
	}
	str = strings.Join(result, " ")
	return
}

// 记录非标准格式的应答
func outputRspUnknown(c *gin.Context) (str string) {
	writer := c.Writer.(*ResponseBodyWriter)
	method := strings.ToUpper(c.Request.Method)
	urlFullPath := c.Request.URL.String()
	urlPath := strings.Split(urlFullPath, "?")[0]
	conf := GetRouterConfig(method, urlPath)
	if conf.DisableRsp {
		return
	}
	result := []string{
		method + "应答",
		fmt.Sprintf("url=%s", urlFullPath),
		fmt.Sprintf("status=%d", writer.Status()),
		fmt.Sprintf("content-type=%s", writer.Header().Get("Content-Type")),
		fmt.Sprintf("size=%d", writer.Size()),
	}
	if conf.OutputRspBody != nil {
		result = append(result, fmt.Sprintf("body=%s", conf.OutputRspBody(c, writer.Body.String())))
	}
	str = strings.Join(result, " ")
	return
}

// 用于替换应答Writer的，记录返回信息
type ResponseBodyWriter struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (w ResponseBodyWriter) Write(b []byte) (int, error) {
	if strings.Contains(w.Header().Get("Content-Type"), "application/json") {
		w.Body.Write(b)
	}
	return w.ResponseWriter.Write(b)
}
