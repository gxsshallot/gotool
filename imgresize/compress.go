package imgresize

import (
	"errors"
	"fmt"
	"image"
	"os"
)

// 执行压缩的函数
func Do(imgFilepath string, params Params) (buf []byte, contentType string, err error) {
	// 处理参数
	if params.Width == 0 && params.Height == 0 {
		buf, err = os.ReadFile(imgFilepath)
		return
	}
	// 读取文件
	file, err := os.Open(imgFilepath)
	if err != nil {
		return
	}
	defer file.Close()
	// 判断类型
	imageConfig, contentType, err := image.DecodeConfig(file)
	if err != nil {
		return
	}
	// 判断类型
	var result Converter = nil
	switch contentType {
	case "jpeg":
		result = ImageConverterJPG{}
	case "png":
		result = ImageConverterPNG{}
	default:
		err = fmt.Errorf("暂不支持%s的格式", contentType)
		return
	}
	// 判断图片是否需要压缩
	needCompress := false
	if params.Width > 0 && imageConfig.Width > int(params.Width) {
		needCompress = true
	}
	if params.Height > 0 && imageConfig.Height > int(params.Height) {
		needCompress = true
	}
	if !needCompress {
		err = errors.New("image size no need to compress")
		return
	}
	// 处理自动缩放
	originWidth, originHeight := imageConfig.Width, imageConfig.Height
	if params.Width == 0 && params.Height > 0 {
		params.Width = uint(int(params.Height) * originWidth / originHeight)
	} else if params.Width > 0 && params.Height == 0 {
		params.Height = uint(int(params.Width) * originHeight / originWidth)
	}
	// 必须新建一个os.File，否则会报错：invalid JPEG format: missing SOI marker
	// 参见https://studygolang.com/articles/15396?fr=sidebar
	fileObj, err := os.Open(imgFilepath)
	if err != nil {
		return
	}
	defer fileObj.Close()
	// 压缩图片
	imageBuf, err := result.Convert(fileObj, params)
	if err != nil {
		return
	}
	buf = imageBuf.Bytes()
	return
}
