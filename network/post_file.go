package network

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strings"
)

/*
发送内部文件请求(POST)，并解析返回值。

	baseUrl：基地址
	apiUrl：URL后面的相对地址
	headers：HTTP头信息，大小写敏感
	requestBodyKey：请求Body存放在表单的哪个键
	requestBody：请求Body
	fileKey：文件存放在表单的哪个键
	fileName：文件名称
	fileReader：文件读取句柄
	hasResponse：是否解析应答
*/
func PostFile[R any](
	baseUrl string,
	apiUrl string,
	headers map[string]interface{},
	requestBodyKey string,
	requestBody interface{},
	fileKey string,
	fileName string,
	fileReader io.Reader,
	hasResponse bool,
) (rsp R, err error) {
	// 生成Body
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	formFile, err := writer.CreateFormFile(fileKey, fileName)
	if err != nil {
		return
	}
	if _, err = io.Copy(formFile, fileReader); err != nil {
		return
	}
	bodyData, err := json.Marshal(requestBody)
	if err != nil {
		return
	}
	if err = writer.WriteField(requestBodyKey, string(bodyData)); err != nil {
		return
	}
	if err = writer.Close(); err != nil {
		return
	}
	// 生成URL
	baseUrl = strings.TrimSuffix(baseUrl, "/")
	apiUrl = strings.TrimPrefix(apiUrl, "/")
	fullUrl := fmt.Sprintf("%s/%s", baseUrl, apiUrl)
	// 生成POST请求
	request, err := http.NewRequest("POST", fullUrl, body)
	if err != nil {
		return
	}
	// 设置Header
	request.Header["Content-Type"] = []string{writer.FormDataContentType()}
	for k, v := range headers {
		request.Header[k] = []string{fmt.Sprintf("%v", v)}
	}
	// 生成Client并发送请求
	client := http.Client{}
	rspHttp, err := client.Do(request)
	if err != nil {
		return
	}
	defer rspHttp.Body.Close()
	// 判断HTTP返回值
	if rspHttp.StatusCode != http.StatusOK {
		err = errors.New(rspHttp.Status)
		return
	}
	// 读取返回值Body
	rspBytes, err := io.ReadAll(rspHttp.Body)
	if err != nil {
		return
	}
	// 解析返回值
	if !hasResponse {
		return
	}
	if err = jsonUnmarshal(rspBytes, &rsp); err != nil {
		return
	}
	return
}
