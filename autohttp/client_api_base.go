package autohttp

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"
)

// 发送HTTP请求的基本流程
func apiBase(
	ctx context.Context,
	item *Item,
) (statusCode int, rspBytes []byte, err error) {
	// 校验数据库回调
	if DatabaseCallback == nil {
		err = errors.New("必须设置数据库回调函数")
		return
	}
	// 查询是否有重复业务
	if item.ID == 0 {
		_, e := DatabaseCallback.TaskDetailByBusiness(ctx, item.BusinessType, item.BusinessId)
		if e == nil {
			err = errors.New("不能有重复的业务")
			return
		}
	}
	// 校验HTTP请求URL
	if len(item.Url) == 0 {
		err = errors.New("HTTP请求URL不能为空")
		return
	}
	// 校验HTTP请求方式
	item.Method = strings.ToUpper(item.Method)
	if item.Method != http.MethodGet &&
		item.Method != http.MethodPost &&
		item.Method != http.MethodPut &&
		item.Method != http.MethodDelete {
		err = fmt.Errorf("不支持的HTTP请求方式%s", item.Method)
		return
	}
	// 校验业务信息
	if len(item.BusinessType) == 0 || len(item.BusinessId) == 0 {
		err = errors.New("业务信息不能为空")
		return
	}
	// 处理状态
	item.Status = StatusSending
	// 首次发起请求/重试请求
	isFirst := item.ID == 0
	now := time.Now().Unix()
	if isFirst {
		item.RequestTime = now
	}
	item.LastRetryTime = now
	item.SuccessTime = 0
	// 校验重试类型
	if isFirst {
		item.RetryType = DefaultRetryType
		item.RetryLadderInterval = RetryLadderDefaultInterval
		item.RetryStepInterval = RetryStepDefaultInterval
		item.RetryStepTime = RetryStepDefaultTime
		item.RetryCount = 0
	} else {
		item.RetryCount += 1
	}
	if item.RetryType {
		if item.RetryStepInterval <= 0 {
			err = errors.New("周期性重试间隔必须大于0")
			return
		}
		if item.RetryStepTime < 0 {
			err = errors.New("周期性重试次数不能为负数")
			return
		}
	} else {
		if len(item.RetryLadderInterval) == 0 {
			err = errors.New("阶梯性重试间隔必须至少包含一项")
			return
		}
	}
	// 处理请求体
	if StatusCallback != nil {
		StatusCallback.OnTaskBuilding(ctx, item)
	}
	// 写入数据库
	if isFirst {
		e := DatabaseCallback.CreateTask(ctx, item)
		if e != nil {
			err = fmt.Errorf("创建任务失败: %s", e.Error())
			return
		}
	} else {
		e := DatabaseCallback.UpdateTask(ctx, item)
		if e != nil {
			err = fmt.Errorf("更新任务状态失败(发送前): %s", e.Error())
			return
		}
	}
	// 状态通知
	if StatusCallback != nil {
		StatusCallback.OnTaskStatusUpdate(ctx, *item, "")
	}
	// 结果处理
	defer func() {
		if err != nil {
			// 发送失败
			item.Status = StatusFailure
			item.SuccessTime = 0
			// 重试信息
			if item.RetryType {
				if item.RetryStepTime != 0 && item.RetryCount >= item.RetryStepTime {
					item.Status = StatusStopped
					item.NextRetryTime = 0
				} else {
					item.NextRetryTime = now + item.RetryStepInterval
				}
			} else {
				if item.RetryCount >= int64(len(item.RetryLadderInterval)) {
					item.Status = StatusStopped
					item.NextRetryTime = 0
				} else {
					item.NextRetryTime = now + item.RetryLadderInterval[item.RetryCount]
				}
			}
			_ = DatabaseCallback.UpdateTask(ctx, item)
			// 状态通知
			if StatusCallback != nil {
				StatusCallback.OnTaskStatusUpdate(ctx, *item, err.Error())
			}
		} else {
			// 发送成功
			item.Status = StatusSuccess
			item.SuccessTime = time.Now().Unix()
			_ = DatabaseCallback.UpdateTask(ctx, item)
			// 状态通知
			if StatusCallback != nil {
				StatusCallback.OnTaskStatusUpdate(ctx, *item, "")
			}
		}
	}()
	// 发送HTTP请求
	statusCode, rspBytes, err = request(item.Method, item.Url, item.Header, bytes.NewReader(item.Body))
	if err != nil {
		err = fmt.Errorf("HTTP请求发送失败: %s", err.Error())
		return
	}
	// 校验结果
	if StatusCallback != nil {
		if err = StatusCallback.OnTaskResultValid(ctx, *item, statusCode, rspBytes); err != nil {
			return
		}
	}
	return
}
