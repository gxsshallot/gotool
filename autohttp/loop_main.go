package autohttp

import (
	"context"
	"time"
)

func init() {
	go loopMain()
}

// 主循环，用于重试HTTP请求
func loopMain() {
	ctx := context.Background()
	for {
		// 间隔
		time.Sleep(time.Duration(LoopInterval) * time.Second)
		// 校验数据库回调
		if DatabaseCallback == nil {
			continue
		}
		// 查询未完成任务
		now := time.Now().Unix()
		taskList, e := DatabaseCallback.IncompleteTaskIdList(ctx, now)
		if e != nil {
			continue
		}
		// 对未完成任务，查询详情并发起重试
		for _, taskId := range taskList {
			taskItem, ee := DatabaseCallback.TaskDetail(ctx, taskId)
			if ee != nil {
				continue
			}
			_, _, _ = apiBase(ctx, &taskItem)
		}
	}
}
