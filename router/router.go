package router

import (
	"gitee.com/gxsshallot/gotool/gin"
)

// 没有REQ或RSP
type NO_OBJ map[string]interface{}

// 通用路由处理函数
type RouterHandler[REQ any, RSP any] func(*gin.Context, REQ) (RSP, error)

// (自动处理返回值) 路由处理流程
func Router[REQ any, RSP any](handler RouterHandler[REQ, RSP]) gin.HandlerFunc {
	return _inner_router(handler, true)
}

// (不处理返回值) 路由处理流程
func RouterNoResponse[REQ any, RSP any](handler RouterHandler[REQ, RSP]) gin.HandlerFunc {
	return _inner_router(handler, false)
}

// 路由处理流程
func _inner_router[REQ any, RSP any](handler RouterHandler[REQ, RSP], hasResponse bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			paramObj REQ
			err      error
		)
		if err = c.ShouldBindJSON(&paramObj); err != nil {
			Failure(c, err)
			return
		}
		rsp, err := handler(c, paramObj)
		if !hasResponse {
			return
		}
		if err != nil {
			Failure(c, err)
			return
		}
		Success(c, rsp)
	}
}
