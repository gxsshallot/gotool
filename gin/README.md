# gin

基于[Gin](https://github.com/gin-gonic/gin)的精简框架，目前使用的是`v1.10.0`的代码。

* 移除多余的文档。
* 仅使用标准`json`解码器。
* 移除`test`相关测试代码。
* 移除`xml`格式解析。
* 移除`protobuf`格式解析。
* 移除`msgpack`格式解析。
* 移除`logger`颜色。
* 移除`validator`校验器。
* 移除`HTML`模板。
