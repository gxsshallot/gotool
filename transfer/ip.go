package transfer

import (
	"bytes"
	"net"
	"strings"
)

// 局域网网段
var (
	AFromIp = net.ParseIP("10.0.0.0")
	AToIp   = net.ParseIP("10.255.255.255")
	BFromIp = net.ParseIP("172.16.0.0")
	BToIp   = net.ParseIP("172.31.255.255")
	CFromIp = net.ParseIP("192.168.0.0")
	CToIp   = net.ParseIP("192.168.255.255")
)

// 检查是否为局域网地址
func IsLAN(addr string) bool {
	parts := strings.Split(addr, ":")
	if len(parts) != 2 {
		return true
	}
	curIp := net.ParseIP(parts[0])
	// 判断当前IP是否为IPv4地址
	if curIp.To4() == nil {
		return true
	}
	return CheckIpRange(curIp, AFromIp, AToIp) || CheckIpRange(curIp, BFromIp, BToIp) || CheckIpRange(curIp, CFromIp, CToIp)
}

// 检查IP是否位于区间中
func CheckIpRange(
	curIp net.IP,
	fromIp net.IP,
	toIp net.IP,
) bool {
	return bytes.Compare(curIp, fromIp) >= 0 && bytes.Compare(curIp, toIp) <= 0
}
