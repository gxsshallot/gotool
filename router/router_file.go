package router

import (
	"fmt"

	"gitee.com/gxsshallot/gotool/gin"
)

// 普通参数键
var RouterFileParamKey = "params"

// (自动处理返回值) 上传文件路由处理流程
func RouterFile[REQ any, RSP any](handler RouterHandler[REQ, RSP]) gin.HandlerFunc {
	return _inner_router_file(handler, true)
}

// (不处理返回值) 上传文件路由处理流程
func RouterFileNoResponse[REQ any, RSP any](handler RouterHandler[REQ, RSP]) gin.HandlerFunc {
	return _inner_router_file(handler, false)
}

// 路由处理流程
func _inner_router_file[REQ any, RSP any](handler RouterHandler[REQ, RSP], hasResponse bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			paramObj REQ
			err      error
		)
		jsonStr, ok := c.GetPostForm(RouterFileParamKey)
		if !ok {
			Failure(c, fmt.Errorf("请使用%s作为参数键", RouterFileParamKey))
			return
		}
		if err = jsonUnmarshal([]byte(jsonStr), &paramObj); err != nil {
			Failure(c, err)
			return
		}
		rsp, err := handler(c, paramObj)
		if !hasResponse {
			return
		}
		if err != nil {
			Failure(c, err)
			return
		}
		Success(c, rsp)
	}
}
