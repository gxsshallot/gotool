package encrypt

import (
	"crypto/md5"
	"encoding/hex"
)

func MD5(plainText string) string {
	hash := md5.New()
	_, _ = hash.Write([]byte(plainText))
	hashSign := hash.Sum(nil)
	return hex.EncodeToString(hashSign)
}
