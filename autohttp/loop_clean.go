package autohttp

import (
	"context"
	"time"
)

func init() {
	go loopClean()
}

// 循环检测待清理项
func loopClean() {
	ctx := context.Background()
	for {
		// 校验数据库回调
		if DatabaseCallback == nil {
			time.Sleep(1 * time.Second)
			continue
		}
		// 判断是否开启循环
		if !AutoCleanEnable {
			time.Sleep(1 * time.Minute)
			continue
		}
		// 查询待清理的项目
		now := time.Now().Unix()
		statusMap := map[uint8]int64{
			StatusFailure: AutoCleanFailureTime,
			StatusSuccess: AutoCleanSuccessTime,
			StatusStopped: AutoCleanStoppedTime,
		}
		taskIdList := []uint64{}
		for status, beforeTime := range statusMap {
			if beforeTime == 0 {
				continue
			}
			list, e := DatabaseCallback.TaskToCleanList(ctx, status, now-beforeTime)
			if e == nil {
				taskIdList = append(taskIdList, list...)
			}
		}
		// 清理项目
		for _, taskId := range taskIdList {
			_ = DatabaseCallback.DeleteTask(ctx, taskId)
		}
		// 间隔
		time.Sleep(time.Duration(AutoCleanLoopInterval) * time.Second)
	}
}
