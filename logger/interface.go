package logger

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
)

// 记录正常信息
func Info(c context.Context, info string, fields ...interface{}) {
	str := fmt.Sprintf(info, fields...)
	logrus.WithContext(c).Info(str)
}

// 记录错误信息
func Error(c context.Context, err error, info string, fields ...interface{}) {
	str := fmt.Sprintf(info, fields...)
	logrus.WithContext(c).WithError(err).Error(str)
}

// 记录错误信息并推出
func Fatal(c context.Context, err error, info string, fields ...interface{}) {
	str := fmt.Sprintf(info, fields...)
	logrus.WithContext(c).WithError(err).Fatal(str)
}
