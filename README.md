# gotool

使用Go语言(>=1.18)编写的，在开发过程中，用到的工具函数集合以及样例。

模块：

* [autohttp](autohttp)：管理HTTP请求的自动重试。
* [config](config)：配置项的获取与监听。
* [cors](cors)：Gin的CORS支持。
* [crontask](crontask)：定时任务。
* [dbtool](dbtool)：数据库工具。
* [docker](docker)：Docker连接管理。
* [encoding](encoding)：编码工具。
* [encrypt](encrypt)：加解密和哈希算法。
* [filedb](filedb)：文件模拟数据库。
* [filesystem](filesystem)：文件系统相关工具。
* [frp](frp)：使用frp管理远程端口。
* [gin](gin)：精简的Gin框架。
* [gomicro](gomicro)：精简的微服务框架。
* [imgresize](imgresize)：图片压缩大小工具。
* [logger](logger)：日志模块。
* [network](network)：网络基础库。
* [password](password)：JWT用户密码。
* [router](router)：路由处理库。
* [sse](sse)：SSE消息通知。
* [token](token)：生成用户Token。
* [transfer](transfer)：传输层工具。
* [types](types)：类型转换。
* [uuid](uuid)：生成UUID。

注意：

* 相关网络工具，基于`gin`框架。
* 相关数据库工具，基于`gorm`框架。
