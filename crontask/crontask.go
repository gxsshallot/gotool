package crontask

import (
	"log"

	"github.com/robfig/cron/v3"
)

type Task struct {
	Name    string // 任务名称
	Time    string // 定时任务的时间字符串，例如"59 59 * * * *"
	Handler func() // 任务的回调函数
}

var (
	tasks []Task
	c     *cron.Cron
)

// 注册定时任务
func Register(task Task) {
	tasks = append(tasks, task)
}

// 启动定时任务
func Init() {
	c = cron.New(cron.WithSeconds())
	for i := 0; i < len(tasks); i++ {
		if _, err := c.AddFunc(tasks[i].Time, tasks[i].Handler); err != nil {
			log.Fatal("添加定时任务出错", err)
		}
	}
	go func() {
		c.Run()
	}()
}
