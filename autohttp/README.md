# autohttp

使用Go语言编写，对于HTTP请求进行发送，如果失败会暂存并重试。

### 使用

在运行模块之前，需要初始化相关配置信息，具体说明参见代码中的注释：

```go
import (
    "gitee.com/gxsshallot/gotool/autohttp"
)

func main() {
    // 设置表名称
    autohttp.DefaultImplementTableName = "xxx"
    // 设置循环处理参数
    autohttp.LoopInterval = 3
    autohttp.AutoCleanEnable = true
    autohttp.AutoCleanLoopInterval = 3600
    autohttp.AutoCleanSuccessTime = 7 * 24 * 3600
    autohttp.AutoCleanFailureTime = 0
    autohttp.AutoCleanStoppedTime = 0
    autohttp.DefaultRetryType = true
    autohttp.RetryLadderDefaultInterval = []int64{}
    autohttp.RetryStepDefaultInterval = 60
    autohttp.RetryStepDefaultTime = 0
    autohttp.TaskAddIfSuccess = true
    // 设置回调
    autohttp.StatusCallback = xxx
    dbcb := autohttp.DefaultImplement{}
    dbcb.SetDB(db)
    autohttp.DatabaseCallback = dbcb
}
```

### 数据库

模块会自动管理数据库，需要在数据库中，新建一张表，至少应包含`id`字段，下表是默认实现的数据库表结构：

| 字段名 | 类型 | 是否主键 | 默认值 | 备注 |
| :-: | :-: | :-: | :-: | :- |
| id | uint64 | 是 | - | 唯一标识ID |
| business_type | string | 否 | - | 业务类型，用户自定义 |
| business_id | string | 否 | - | 业务主键，同一业务类型不能出现重复 |
| url | string | 否 | - | HTTP请求URL |
| method | string | 否 | - | HTTP请求方法 |
| header | string | 否 | - | HTTP请求头 |
| body | string | 否 | - | HTTP请求Body |
| retry_type | bool | 否 | - | 重试类型 |
| retry_ladder_interval | string | 否 | - | 阶梯性重试的间隔，使用逗号分隔 |
| retry_step_interval | int64 | 否 | - | 周期性重试的间隔 |
| retry_step_time | int64 | 否 | - | 周期性重试的次数 |
| retry_count | int64 | 否 | - | 重试计数 |
| status | uint8 | 否 | - | 状态，参见常量StatusXXX |
| request_time | int64 | 否 | - | 首次发起请求时间，Unix时间戳，单位秒 |
| last_retry_time | int64 | 否 | - | 上一次重试的时间，Unix时间戳，单位秒 |
| next_retry_time | int64 | 否 | - | 下一次重试时间，Unix时间戳，单位秒 |
| success_time | int64 | 否 | - | 成功时间，Unix时间戳，单位秒 |
