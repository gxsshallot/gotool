package config

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
)

// 监听项配置
type ListenItem struct {
	Filename string                // 文件名，不包含后缀名
	Target   interface{}           // 赋值对象，指针
	OnChange func(filename string) // 发生变化的回调函数
}

// 启动监听
func Init(baseDir string, items []ListenItem) {
	// 检查目录
	if info, e := os.Stat(baseDir); e != nil {
		logrus.WithError(e).Fatalf("目录%s不存在", baseDir)
	} else if !info.IsDir() {
		logrus.Fatalf("%s不是目录", baseDir)
	}
	// 初始化监听器
	w, e := fsnotify.NewWatcher()
	if e != nil {
		logrus.WithError(e).Fatal("启动配置文件监听失败")
	}
	// 添加监听项
	kvMap := map[string]ListenItem{}
	for _, item := range items {
		e := ReadToml(baseDir, item.Filename, item.Target)
		if e != nil {
			logrus.WithError(e).Fatalf("%s配置项获取失败", item.Filename)
		}
		w.Add(filepath.Join(baseDir, item.Filename+".toml"))
		kvMap[item.Filename] = item
	}
	// 轮询监听
	go func() {
		for {
			select {
			case event := <-w.Events:
				logrus.Infof("文件(%s)监听变化: %s", event.Name, event.Op.String())
				_, filename := filepath.Split(event.Name)
				if !strings.HasSuffix(filename, ".toml") {
					continue
				}
				filename = strings.TrimSuffix(filename, ".toml")
				if item, ok := kvMap[filename]; ok {
					OnChange(baseDir, item)
				}
			case err := <-w.Errors:
				logrus.WithError(err).Error("监听配置文件错误")
			case <-time.After(1 * time.Minute):
				continue
			}
		}
	}()
}

// 监听数据项变化
func OnChange(baseDir string, item ListenItem) {
	logrus.Infof("%s配置项发生变化", item.Filename)
	// 解码新的配置值
	if err := ReadToml(baseDir, item.Filename, item.Target); err != nil {
		logrus.WithError(err).Errorf("%s配置项Json解码失败", item.Filename)
	} else {
		logrus.Infof("%s配置项Json解码成功", item.Filename)
	}
	// 回调通知
	if item.OnChange != nil {
		item.OnChange(item.Filename)
	}
}

// 读取TOML文件内容
func ReadToml(baseDir string, filename string, target interface{}) (err error) {
	fullpath := filepath.Join(baseDir, filename+".toml")
	content, err := os.ReadFile(fullpath)
	if err != nil {
		return
	}
	if err = toml.Unmarshal(content, target); err != nil {
		return
	}
	return
}
