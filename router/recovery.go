package router

import (
	"fmt"
	"net"
	"os"
	"runtime"
	"strings"

	"gitee.com/gxsshallot/gotool/gin"
	"github.com/sirupsen/logrus"
)

// 恢复panic的中间件
func Recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}
				// If the connection is dead, we can't write a status to it.
				stack := getStack()
				logrus.WithContext(c).WithError(err.(error)).Errorf("runtime stack = \n%s", stack)
				if brokenPipe {
					_ = c.Error(err.(error))
					c.Abort()
				} else {
					Failure(c, err.(error))
				}
			}
		}()
		c.Next()
	}
}

func getStack() (stack string) {
	ptr := make([]uintptr, 100)
	length := runtime.Callers(2, ptr) // skip "runtime.Callers", "getStack"
	frames := runtime.CallersFrames(ptr[:length])
	for {
		item, more := frames.Next()
		if !more {
			break
		}
		stack += fmt.Sprintf("%s:\n\t%s:%d (0x%x)\n", item.Function, item.File, item.Line, item.Entry)
	}
	stack = strings.TrimSuffix(stack, "\n")
	return
}
