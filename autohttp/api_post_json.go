package autohttp

import (
	"context"
	"encoding/json"
	"net/http"
)

// POST请求，application/json
func PostJson(
	ctx context.Context,
	businessType string,
	businessId string,
	url string,
	header map[string]string,
	body interface{},
) (statusCode int, rspBytes []byte, err error) {
	bodyData, err := json.Marshal(body)
	if err != nil {
		return
	}
	if header == nil {
		header = map[string]string{}
	}
	if _, ok1 := header["Content-Type"]; !ok1 {
		if _, ok2 := header["content-type"]; !ok2 {
			header["Content-Type"] = "application/json"
		}
	}
	item := Item{
		BusinessType: businessType,
		BusinessId:   businessId,
		Url:          url,
		Method:       http.MethodPost,
		Header:       header,
		Body:         bodyData,
	}
	statusCode, rspBytes, err = apiBase(ctx, &item)
	return
}
