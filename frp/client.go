package frp

import (
	"reflect"
	"time"

	"github.com/fatedier/frp/client"
)

// 本地处理类
type Instance struct {
	LastCmd Command         // 最近的命令
	Svr     *client.Service // frpc服务
}

// 收到新的sse命令
func (ins *Instance) Reload(cmd Command) (err error) {
	if ins.Svr == nil {
		if len(cmd.Proxy) > 0 {
			return ins.startService(cmd)
		} else {
			return nil
		}
	} else {
		isNew, isReload, isClose := ins.compareCommand(cmd)
		if isNew {
			ins.closeService()
			if isClose {
				return nil
			} else {
				return ins.startService(cmd)
			}
		} else if isClose {
			ins.closeService()
			return nil
		} else if isReload {
			return ins.reloadService(cmd)
		} else {
			return nil
		}
	}
}

// 新建frp服务
func (ins *Instance) startService(cmd Command) (err error) {
	cfg, pxyCfgs := cmd.Generate()
	svr, err := client.NewService(cfg, pxyCfgs, nil, "")
	if err != nil {
		return
	}
	go svr.Run()
	ins.Svr = svr
	ins.LastCmd = cmd
	return
}

// 刷新frp服务
func (ins *Instance) reloadService(cmd Command) (err error) {
	_, newPxyCfgs := cmd.Generate()
	if err = ins.Svr.ReloadConf(newPxyCfgs, nil); err != nil {
		return
	}
	ins.LastCmd = cmd
	return
}

// 关闭frp服务
func (ins *Instance) closeService() {
	if ins.Svr != nil {
		ins.Svr.Close()
		ins.Svr = nil
		time.Sleep(250 * time.Millisecond)
	}
}

// 对比命令，返回(是否新建,是否刷新,是否关闭)
func (ins Instance) compareCommand(cmd Command) (bool, bool, bool) {
	isNew := !reflect.DeepEqual(cmd.Client, ins.LastCmd.Client)
	isReload := !reflect.DeepEqual(cmd.Proxy, ins.LastCmd.Proxy)
	isClose := len(cmd.Proxy) == 0
	return isNew, isReload, isClose
}
