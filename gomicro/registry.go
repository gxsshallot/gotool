package gomicro

type RegistryService struct {
	Name      string              `json:"name"`
	Version   string              `json:"version"`
	Metadata  map[string]string   `json:"metadata"`
	Endpoints []*RegistryEndpoint `json:"endpoints"`
	Nodes     []*RegistryNode     `json:"nodes"`
}

type RegistryNode struct {
	Metadata map[string]string `json:"metadata"`
	Id       string            `json:"id"`
	Address  string            `json:"address"`
}

type RegistryEndpoint struct {
	Request  *RegistryValue    `json:"request"`
	Response *RegistryValue    `json:"response"`
	Metadata map[string]string `json:"metadata"`
	Name     string            `json:"name"`
}

type RegistryValue struct {
	Name   string           `json:"name"`
	Type   string           `json:"type"`
	Values []*RegistryValue `json:"values"`
}
