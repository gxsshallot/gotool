package encrypt

import "crypto/aes"

// AES/ECB/PKCS7模式加密
func AesEbcPkcs7Encrypt(plainText []byte, key string) (cipherText []byte, err error) {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return
	}
	ecb := NewECBEncrypter(block)
	content := PKCSPadding(plainText, block.BlockSize())
	cipherText = make([]byte, len(content))
	ecb.CryptBlocks(cipherText, content)
	return
}
