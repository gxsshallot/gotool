# token

用于生成用户在前端页面中的Token字段。

接口：

* `GenerateToken`：生成一个新的Token信息。
* `ParseToken`：解析已有的Token信息。

### Token

例如声明一个`jwt.Claims`接口的实现：

```go
type CustomClaims struct {
	jwt.StandardClaims
	UserId     uint64 `json:"uid"` // 员工ID
	Env        string `json:"ev"`  // 环境信息
	ExpireTime int64  `json:"et"`  // 有效截止时间的时间戳
}
```

调用生成Token函数：

```go
claims := CustomClaims{UserId: 1, Env: "web", ExpireTime: time.Now().Unix()}
token, err := GenerateToken[*CustomClaims](&claims, "1234567890")
```

调用解析Token函数：

```go
var gotClaims CustomClaims
err = ParseToken[*CustomClaims](token, "1234567890", &gotClaims)
```
