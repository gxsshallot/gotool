package autohttp

// 数据库项的通用格式
type Item struct {
	ID                  uint64            // 唯一标识ID
	BusinessType        string            // 业务类型，用户自定义
	BusinessId          string            // 业务主键，同一业务类型不能出现重复
	Url                 string            // HTTP请求URL
	Method              string            // HTTP请求方法
	Header              map[string]string // HTTP请求头
	Body                []byte            // HTTP请求Body
	RetryType           bool              // 重试类型
	RetryLadderInterval []int64           // 阶梯性重试的间隔
	RetryStepInterval   int64             // 周期性重试的间隔
	RetryStepTime       int64             // 周期性重试的次数
	RetryCount          int64             // 重试计数
	Status              uint8             // 状态，参见常量StatusXXX
	RequestTime         int64             // 首次发起请求时间，Unix时间戳，单位秒
	LastRetryTime       int64             // 上一次重试的时间，Unix时间戳，单位秒
	NextRetryTime       int64             // 下一次重试时间，Unix时间戳，单位秒
	SuccessTime         int64             // 成功时间，Unix时间戳，单位秒
}

// 传输状态
const (
	StatusSending uint8 = iota + 1 // 发送中
	StatusSuccess                  // 成功
	StatusFailure                  // 失败
	StatusStopped                  // 停止重试
)
