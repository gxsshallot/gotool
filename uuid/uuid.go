package uuid

import (
	"encoding/hex"

	"github.com/google/uuid"
)

// 生成32位UUID
func String32() string {
	u := uuid.New()
	var buf [32]byte
	hex.Encode(buf[:], u[:])
	return string(buf[:])
}
