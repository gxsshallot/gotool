package encrypt

import (
	"crypto/sha1"
	"encoding/hex"
)

func SHA1(plainText string) string {
	h := sha1.New()
	_, _ = h.Write([]byte(plainText))
	return hex.EncodeToString(h.Sum([]byte("")))
}
