package dbtool

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

// MySQL的配置信息
type MysqlConfig struct {
	Host              string            // 服务器主机地址
	Port              int               // 服务器端口号
	User              string            // 服务器登陆用户
	Password          string            // 调试时数据库密码
	Scheme            string            // 数据库名称
	MaxIdleConnection int               // 最大闲置连接数
	MaxOpenConnection int               // 最大打开连接数
	DebugMode         bool              // 开启SQL调试模式
	Params            map[string]string // 连接参数
}

// 获取MySQL数据库连接
func ConnectMysql(config MysqlConfig) (db *gorm.DB, err error) {
	// 参数拼接
	params := []string{}
	for k, v := range config.Params {
		params = append(params, fmt.Sprintf("%s=%s", k, v))
	}
	// 数据库连接
	dataSource := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.Scheme,
		strings.Join(params, "&"),
	)
	db, err = gorm.Open("mysql", dataSource)
	if err != nil {
		return
	}
	// 最大连接数
	if config.MaxOpenConnection > 0 {
		db.DB().SetMaxOpenConns(config.MaxOpenConnection)
	}
	// 最大闲置数
	if config.MaxIdleConnection > 0 {
		db.DB().SetMaxIdleConns(config.MaxIdleConnection)
	}
	return
}
