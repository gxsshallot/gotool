package autohttp

import "context"

// 数据库操作类回调函数
type DatabaseCallbackInterface interface {
	// 创建初始任务
	CreateTask(c context.Context, item *Item) (err error)
	// 删除任务
	DeleteTask(c context.Context, taskId uint64) (err error)
	// 查询待重试的任务(仅ID)
	IncompleteTaskIdList(c context.Context, currentTime int64) (list []uint64, err error)
	// 待清理任务列表，status表示状态，timestamp表示某个Unix时间点之前
	TaskToCleanList(c context.Context, status uint8, timestamp int64) (list []uint64, err error)
	// 查询任务详细信息
	TaskDetail(c context.Context, taskId uint64) (item Item, err error)
	// 查询任务详细信息
	TaskDetailByBusiness(c context.Context, businessType string, businessId string) (item Item, err error)
	// 更新任务
	UpdateTask(c context.Context, item *Item) (err error)
}

// 全局数据库回调方法
var DatabaseCallback DatabaseCallbackInterface = nil
