package dbtool

import (
	"database/sql"
	"errors"

	"github.com/jinzhu/gorm"
)

// 事务实际操作函数
type TransactionFunc[T any] func(*gorm.DB) (T, error)

// GORM事务处理
func Transaction[T any](db *gorm.DB, f TransactionFunc[T]) (data T, err error) {
	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
			tx.Rollback()
		}
	}()
	data, err = f(tx)
	if err != nil {
		tx.Rollback()
		return
	}
	tx.Commit()
	return
}

// 判断是否是事务句柄
func IsTransaction(db *gorm.DB) error {
	if db == nil {
		return errors.New("数据库对象为空")
	}
	_, ok := db.CommonDB().(*sql.Tx)
	if !ok {
		return errors.New("数据库对象不是事务")
	}
	return nil
}
