# filesystem

文件系统相关工具。

* `CopyFile`：拷贝文件。
* `CreateDir`：生成目录。
* `IsFile`：判断是否是文件。
* `IsDir`：判断是否是目录。
* `ReadFileList`：批量读取目录中的大量文件。
