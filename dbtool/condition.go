package dbtool

import (
	"fmt"
	"reflect"
	"strings"

	"golang.org/x/exp/constraints"
)

// 查询的拼接结果
type ConditionItem struct {
	WhereSql  string
	WhereArgs []interface{}
}

// 文本字段(模糊搜索)
func FieldTextLike(name string, value string) *ConditionItem {
	if len(value) == 0 {
		return nil
	}
	return &ConditionItem{
		WhereSql:  fmt.Sprintf("%s LIKE ?", name),
		WhereArgs: []interface{}{"%" + value + "%"},
	}
}

// 枚举字段包含关系
func FieldEnumIn[T constraints.Ordered](
	name string,
	value []T,
) *ConditionItem {
	if len(value) == 0 {
		return nil
	}
	if reflect.TypeOf(value[0]).Kind() == reflect.Uint8 {
		valueStrs := make([]string, len(value))
		for i, v := range value {
			valueStrs[i] = fmt.Sprintf("%v", v)
		}
		arrStr := strings.Join(valueStrs, ",")
		return &ConditionItem{
			WhereSql:  fmt.Sprintf("%s in (%s)", name, arrStr),
			WhereArgs: nil,
		}
	} else {
		return &ConditionItem{
			WhereSql:  fmt.Sprintf("%s in (?)", name),
			WhereArgs: []interface{}{value},
		}
	}
}

// 枚举字段不包含关系
func FieldEnumNotIn[T constraints.Ordered](
	name string,
	value []T,
) *ConditionItem {
	if len(value) == 0 {
		return nil
	}
	if reflect.TypeOf(value[0]).Kind() == reflect.Uint8 {
		valueStrs := make([]string, len(value))
		for i, v := range value {
			valueStrs[i] = fmt.Sprintf("%v", v)
		}
		arrStr := strings.Join(valueStrs, ",")
		return &ConditionItem{
			WhereSql:  fmt.Sprintf("%s not in (%s)", name, arrStr),
			WhereArgs: nil,
		}
	} else {
		return &ConditionItem{
			WhereSql:  fmt.Sprintf("%s not in (?)", name),
			WhereArgs: []interface{}{value},
		}
	}
}

// 可比较字段(等于)，包含数字和字符串
func FieldOrderedEqual[T constraints.Ordered](
	name string,
	value T,
) *ConditionItem {
	return fieldOrdered(name, value, "=")
}

// 可比较字段(小于)，包含数字和字符串
func FieldOrderedLess[T constraints.Ordered](
	name string,
	value T,
) *ConditionItem {
	return fieldOrdered(name, value, "<")
}

// 可比较字段(小于等于)，包含数字和字符串
func FieldOrderedLessEqual[T constraints.Ordered](
	name string,
	value T,
) *ConditionItem {
	return fieldOrdered(name, value, "<=")
}

// 可比较字段(大于)，包含数字和字符串
func FieldOrderedMore[T constraints.Ordered](
	name string,
	value T,
) *ConditionItem {
	return fieldOrdered(name, value, ">")
}

// 可比较字段(大于等于)，包含数字和字符串
func FieldOrderedMoreEqual[T constraints.Ordered](
	name string,
	value T,
) *ConditionItem {
	return fieldOrdered(name, value, ">=")
}

// 可比较字段内部实现，包含数字和字符串
func fieldOrdered[T constraints.Ordered](
	name string,
	value T,
	operator string,
) *ConditionItem {
	var emptyValue T
	if value == emptyValue {
		return nil
	}
	return &ConditionItem{
		WhereSql:  fmt.Sprintf("%s %s ?", name, operator),
		WhereArgs: []interface{}{value},
	}
}

// AND连接符
func ConnAnd(items ...*ConditionItem) *ConditionItem {
	return _conn("and", items...)
}

// OR连接符
func ConnOr(items ...*ConditionItem) *ConditionItem {
	return _conn("or", items...)
}

// 内部连接符函数
func _conn(conn string, items ...*ConditionItem) *ConditionItem {
	sqlItems, sqlArgs := []string{}, []interface{}{}
	for _, item := range items {
		if item != nil && len(item.WhereSql) > 0 {
			sqlItems = append(sqlItems, fmt.Sprintf("(%s)", item.WhereSql))
			sqlArgs = append(sqlArgs, item.WhereArgs...)
		}
	}
	if len(sqlItems) == 0 {
		return nil
	}
	return &ConditionItem{
		WhereSql:  strings.Join(sqlItems, fmt.Sprintf(" %s ", conn)),
		WhereArgs: sqlArgs,
	}
}
