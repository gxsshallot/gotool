package dbtool

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

// 分页查询列表的公共参数
type ListByPageParams struct {
	PageNumber uint64            `json:"page_number"` // 从1开始的页码
	PageSize   uint64            `json:"page_size"`   // 大于1为有效的页大小
	Sort       map[string]string `json:"sort"`        // 排序信息(仅支持一个)，[字段名:ascend/descend]
}

// 分页查询
func ListByPage(db *gorm.DB, params *ListByPageParams) *gorm.DB {
	if params == nil {
		return db
	}
	sortList := []string{}
	for k, v := range params.Sort {
		if v == "ascend" {
			sortList = append(sortList, fmt.Sprintf("%s ASC", k))
		} else if v == "descend" {
			sortList = append(sortList, fmt.Sprintf("%s DESC", k))
		}
	}
	if len(sortList) > 0 {
		db = db.Order(strings.Join(sortList, ","))
	}
	if params.PageNumber > 0 && params.PageSize > 0 {
		offset := (params.PageNumber - 1) * params.PageSize
		db = db.Offset(offset).Limit(params.PageSize)
	}
	return db
}
