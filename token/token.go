package token

import (
	"github.com/dgrijalva/jwt-go"
)

// 生成Token
func GenerateToken[T jwt.Claims](claim T, secretKey string) (tokenString string, err error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenString, err = token.SignedString([]byte(secretKey))
	return
}

// 解析Token
func ParseToken[T jwt.Claims](tokenString string, secretKey string, targetClaims T) (err error) {
	_, err = jwt.ParseWithClaims(tokenString, targetClaims, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(secretKey), nil
	})
	return
}
